package pl.kayon.driveby.mbus;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by rafalmodrzynski on 17.01.2018.
 */

public class FrameBuilderTest {

    @Test
    public void frameBuilderAppend() throws Exception {
        FrameBuilder b = FrameBuilder.getInstance();
        String data = "4D 4C 44 50 AA AA AA AA 21 01 01 02 CD 55 55 55 55 24 1D 44 01 06 48 06 00 88 02 02 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 AA AA AA AA 21 01 01 02 CD ";
        data = data.replace(" ", "");
        byte[] b0 = new byte[data.length()/2];
        for (int i=0; i<b0.length; i++) {
            b0[i] = (byte) Integer.parseInt(data.substring(i*2, i*2 + 2), 16);
        }
        ArrayList<KayonFrame> frames = b.append(b0);
        Assert.assertEquals(3, frames.size());
        byte[] b1 = new byte[]{(byte) 0xAA, 0x55, 0x00, 0x55};
        byte[] b2 = new byte[]{0x55, 0x55, 0x55, 0x12, 0x08};
        byte[] b3 = new byte[]{0x44, 2, 3, 4, 5, 6, 7, 8};
        Assert.assertEquals(0, b.append(b1).size());
        Assert.assertEquals(0, b.append(b2).size());
        Assert.assertEquals(1, b.append(b3).size());
    }
}
