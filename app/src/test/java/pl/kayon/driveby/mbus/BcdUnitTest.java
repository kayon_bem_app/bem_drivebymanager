package pl.kayon.driveby.mbus;

import org.junit.Test;

import pl.kayon.driveby.mbus.Bcd;

import static org.junit.Assert.assertEquals;

/**
 * Created by rafalmodrzynski on 11.01.2018.
 */

public class BcdUnitTest {

    @Test
    public void parseBcd_isCorrect() throws Exception {
        byte[] t1 = new byte[]{0x57, 0x61, 0x01, 0x00, 0x00, 0x00};
        System.out.println(new Bcd(t1).toString());
        System.out.println(new Bcd(t1).longValue());
//        assertEquals(1, Bcd.parseBcd("01").intValue());
//        assertEquals(1123, Bcd.parseBcd("1123").intValue());
//        ExpectedException exception = ExpectedException.none();
//        exception.expect(NumberFormatException.class);
//        Bcd.parseBcd("112").intValue();
        byte x = 51;
        System.out.println(String.format("%X", x));
    }
}
