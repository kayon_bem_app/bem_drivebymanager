package pl.kayon.driveby;

import org.junit.Test;

import pl.kayon.driveby.mbus.DeviceType;
import pl.kayon.driveby.mbus.MeterFrame;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);

//        byte[] t_data = {0x00, 0x49, 0x07, 0x3B, 0x22, 0x40}; //2017-01-27 07:09:00
        byte[] t_data = {0x1E, 0x48, 0x07, 0x3B, 0x22, 0x40}; //2017-01-27 07:08:30
        if ((t_data[1] & 0x80) == 0) {     // Time valid ?
            int tm_sec = t_data[0] & 0x3F;
            int tm_min = t_data[1] & 0x3F;
            int tm_hour = t_data[2] & 0x1F;
            int tm_mday = t_data[3] & 0x1F;
            int tm_mon = (t_data[4] & 0x0F) - 1;
            int tm_year = 1900 + 100 + (((t_data[3] & 0xE0) >> 5) |
                    ((t_data[4] & 0xF0) >> 1));
//            int tm_isdst = (t_data[0] & 0x40) ? 1 : 0;  // day saving time
//            System.out.println(tm_sec);
//            System.out.println(tm_min);
//            System.out.println(tm_hour);
//            System.out.println(tm_mday);
//            System.out.println(tm_mon);
//            System.out.println(tm_year);
            System.out.println(String.format("%04d-%02d-%02d %02d:%02d:%02d", tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec));
        }

    }

    @Test
    public void mbus() throws Exception {
        byte[] header = new byte[]{(byte) 0x55, (byte) 0x55, (byte) 0x55, (byte) 0x55, (byte) 0x1d, (byte) 0x9e, (byte) 0x44, (byte) 0x01, (byte) 0x06, (byte) 0x48, (byte) 0x01, (byte) 0x00, (byte) 0x88, (byte) 0x02, (byte) 0x02, (byte) 0x6e, (byte) 0x1d, (byte) 0x7a, (byte) 0x00, (byte) 0x10, (byte) 0x90, (byte) 0x05, (byte) 0x6d, (byte) 0x71, (byte) 0x2c, (byte) 0xd5, (byte) 0x30, (byte) 0xef, (byte) 0x09, (byte) 0xac, (byte) 0xa3, (byte) 0xa2, (byte) 0xe0, (byte) 0x04, (byte) 0xc1, (byte) 0x3b, (byte) 0xf8, (byte) 0xdc, (byte) 0x92, (byte) 0x6c, (byte) 0xd9, (byte) 0x62, (byte) 0x3d, (byte) 0xdc, (byte) 0xfc, (byte) 0x8a, (byte) 0x18, (byte) 0x6c, (byte) 0xc1, (byte) 0xb0, (byte) 0xed, (byte) 0xa0, (byte) 0x15, (byte) 0xd5, (byte) 0x9a, (byte) 0xe5, (byte) 0x47, (byte) 0xa2, (byte) 0x35, (byte) 0xe3, (byte) 0x32, (byte) 0xe6, (byte) 0xf3, (byte) 0xf3, (byte) 0xfe, (byte) 0x5f, (byte) 0x9f, (byte) 0x73, (byte) 0x05, (byte) 0x93, (byte) 0x21, (byte) 0x4d, (byte) 0xcb, (byte) 0x65, (byte) 0x41, (byte) 0x2b, (byte) 0x89, (byte) 0x1e, (byte) 0x78, (byte) 0xc7, (byte) 0x31, (byte) 0x11, (byte) 0x7b, (byte) 0xcd, (byte) 0x51, (byte) 0x67, (byte) 0xab, (byte) 0x33, (byte) 0x30, (byte) 0x86, (byte) 0x78, (byte) 0x34, (byte) 0x2e, (byte) 0xb5, (byte) 0x88, (byte) 0x71, (byte) 0xd8, (byte) 0x38, (byte) 0xff, (byte) 0x0b, (byte) 0x26, (byte) 0x3a, (byte) 0x39, (byte) 0xd2, (byte) 0xc5, (byte) 0xf5, (byte) 0x66, (byte) 0xcb, (byte) 0xd8, (byte) 0x3e, (byte) 0x77, (byte) 0x03, (byte) 0x9a, (byte) 0xf5, (byte) 0x67, (byte) 0xbf, (byte) 0x2d, (byte) 0x32, (byte) 0xba, (byte) 0xc7, (byte) 0xa7, (byte) 0xba, (byte) 0x4d, (byte) 0x9f, (byte) 0xbf, (byte) 0x30, (byte) 0x0a, (byte) 0x96, (byte) 0x90, (byte) 0x90, (byte) 0xcf, (byte) 0x6e, (byte) 0x4f, (byte) 0x4d, (byte) 0x46, (byte) 0xcf, (byte) 0x73, (byte) 0xae, (byte) 0x6d, (byte) 0x98, (byte) 0x44, (byte) 0xff, (byte) 0x76, (byte) 0x88, (byte) 0xa4, (byte) 0x8a, (byte) 0xfa, (byte) 0xc2, (byte) 0x00, (byte) 0x44, (byte) 0x68, (byte) 0xcc, (byte) 0x6c, (byte) 0x1a, (byte) 0xe4, (byte) 0x53, (byte) 0x19, (byte) 0xf6, (byte) 0x3a, (byte) 0xf1, (byte) 0xbf, (byte) 0x7c, (byte) 0x40, (byte) 0xbf};
        MeterFrame f = new MeterFrame(header, 0);
        assertEquals(29, f.getRssi());
        assertEquals(DeviceType.ELECTRICITY_METER, f.getMbusData().getDeviceType());
        assertEquals("88000148", f.getMbusData().getWmbus().toString());
        assertEquals("APA", f.getMbusData().getManufactureCode());
        assertEquals(2, f.getMbusData().getVersion());
    }

    @Test
    public void rest() throws Exception {
//        Route[] routes = NetworkUtilities.getRoutes();
//        assertEquals(3, routes.length);
    }

}
