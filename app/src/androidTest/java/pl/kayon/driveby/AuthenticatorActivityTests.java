package pl.kayon.driveby;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.view.View;

import pl.kayon.driveby.gui.auth.AuthenticatorActivity;

/**
 * Created by rafalmodrzynski on 21.01.2018.
 */

@SmallTest
public class AuthenticatorActivityTests extends
                                        ActivityInstrumentationTestCase2<AuthenticatorActivity> {

    private static final int ACTIVITY_WAIT = 10000;

    private Instrumentation mInstrumentation;

    private Context mContext;

    public AuthenticatorActivityTests() {

        super(AuthenticatorActivity.class);
    }

    /**
     * Common setup code for all tests. Sets up a default launch intent, which
     * some tests will use (others will override).
     */
    @Override
    protected void setUp() throws Exception {

        super.setUp();
        mInstrumentation = this.getInstrumentation();
        mContext = mInstrumentation.getTargetContext();
    }

    @Override
    protected void tearDown() throws Exception {

        super.tearDown();
    }

    /**
     * Confirm that Login is presented.
     */
    @SmallTest
    public void testLoginOffered() {
        Instrumentation.ActivityMonitor monitor =
                mInstrumentation.addMonitor(AuthenticatorActivity.class.getName(), null, false);
        Intent intent = new Intent(mContext, AuthenticatorActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mInstrumentation.startActivitySync(intent);
        Activity activity = mInstrumentation.waitForMonitorWithTimeout(monitor, ACTIVITY_WAIT);
        View loginButton = activity.findViewById(R.id.btn_login);
        assertEquals(View.VISIBLE, loginButton.getVisibility());
    }
}
