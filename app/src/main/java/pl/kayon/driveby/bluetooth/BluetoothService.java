package pl.kayon.driveby.bluetooth;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanCallback;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.util.List;
import java.util.UUID;

/**
 * Created by rafalmodrzynski on 13.01.2018.
 */

public class BluetoothService extends Service {
    //Strings representing actions to broadcast to activities
    public static final String MLDP_PRIVATE_SERVICE = "00035b03-58e6-07dd-021a-08123a000300"; //Private service for Microchip MLDP
    public static final String MLDP_DATA_PRIVATE_CHAR = "00035b03-58e6-07dd-021a-08123a000301"; //Characteristic for MLDP Data, properties - notify, write
    public static final String MLDP_CONTROL_PRIVATE_CHAR = "00035b03-58e6-07dd-021a-08123a0003ff"; //Characteristic for MLDP Control, properties - read, write
    public static final String CHARACTERISTIC_NOTIFICATION_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";    //Special UUID for descriptor needed to enable notifications
    public static final String ACTION_GATT_CONNECTED = "com.microchip.rn4020die2.ACTION_GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = "com.microchip.rn4020die2.ACTION_GATT_DISCONNECTED";
    public static final String ACTION_GATT_SERVICES_DISCOVERED = "com.microchip.rn4020die2.ACTION_GATT_SERVICES_DISCOVERED";
    public static final String ACTION_DATA_AVAILABLE = "com.microchip.rn4020die2.ACTION_DATA_AVAILABLE";
    public static final String ACTION_DATA_WRITTEN = "com.microchip.rn4020die2.ACTION_DATA_WRITTEN";
    public static final String EXTRA_DATA = "com.microchip.rn4020die2.EXTRA_DATA";
    public static final UUID UUID_MLDP_DATA_PRIVATE_CHARACTERISTIC = UUID.fromString(MLDP_DATA_PRIVATE_CHAR);
    public static final UUID UUID_CHARACTERISTIC_NOTIFICATION_CONFIG = UUID.fromString(CHARACTERISTIC_NOTIFICATION_CONFIG);
    private static final String TAG = BluetoothService.class.getSimpleName(); //Get name of service to tag debug and warning messages
    private final IBinder mBinder = new LocalBinder(); //Binder for Activity that binds to this Service
    private BluetoothManager mBluetoothManager; //BluetoothManager used to get the BluetoothAdapter
    private BluetoothAdapter mBluetoothAdapter; //The BluetoothAdapter controls the BLE radio in the phone/tablet
    private BluetoothGatt mBluetoothGatt; //BluetoothGatt controls the Bluetooth communication link
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) { //Change in connection state
            if (newState == BluetoothProfile.STATE_CONNECTED) { //See if we are connected
                broadcastUpdate(ACTION_GATT_CONNECTED); //Go broadcast an intent to say we are connected
                Log.i(TAG, "Connected to GATT server, starting service discovery");
                mBluetoothGatt.discoverServices(); //Discover services on connected BLE device
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) { //See if we are not connected
                mBluetoothGatt.close();
                mBluetoothGatt = null;
                broadcastUpdate(ACTION_GATT_DISCONNECTED); //Go broadcast an intent to say we are disconnected
                Log.i(TAG, "Disconnected from GATT server.");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) { //BLE service discovery complete
            if (status == BluetoothGatt.GATT_SUCCESS) { //See if the service discovery was successful
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED); //Go broadcast an intent to say we have discovered services
            } else { //Service discovery failed so log a warning
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        //For information only. This application uses Indication to receive updated characteristic data, not Read
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) { //A request to Read has completed
            if (status == BluetoothGatt.GATT_SUCCESS) { //See if the read was successful
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic); //Go broadcast an intent with the characteristic data
            }
        }

        //For information only. This application sends small packets infrequently and does not need to know what the previous write completed
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) { //A request to Write has completed
            if (status == BluetoothGatt.GATT_SUCCESS) { //See if the write was successful
                broadcastUpdate(ACTION_DATA_WRITTEN, characteristic); //Go broadcast an intent to say we have have written data
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) { //Indication or notification was received
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic); //Go broadcast an intent with the characteristic data
        }
    };
    private String mBluetoothDeviceAddress; //Address of the connected BLE device

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder; //Return LocalBinder when an Activity binds to this Service
    }

    @Override
    public boolean onUnbind(Intent intent) {
        if (mBluetoothGatt != null) { //Check for existing BluetoothGatt connection
            mBluetoothGatt.close(); //Close BluetoothGatt coonection for proper cleanup
            mBluetoothGatt = null; //No longer have a BluetoothGatt connection
        }
        return super.onUnbind(intent);
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action); //Create new intent to broadcast the action
        sendBroadcast(intent); //Broadcast the intent
    }

    /**
     * Broadcast an intent with a string representing an action an extra string with the data
     * Modify this code for data that is not in a string format
     *
     * @param action
     * @param characteristic
     */
    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action); //Create new intent to broadcast the action
        if (action.equals(ACTION_DATA_AVAILABLE)) { //See if we need to send data
            if (UUID_MLDP_DATA_PRIVATE_CHARACTERISTIC.equals(characteristic.getUuid())) { //See if this is the correct characteristic
                byte[] dataValue = characteristic.getValue();
                StringBuilder b = new StringBuilder();
                for (byte d : dataValue) {
                    b.append(String.format("%02X ", (d & 0xFF)));
                }
                Log.d(TAG, "incomingPacket: " + b.toString());
                intent.putExtra(EXTRA_DATA, dataValue);
            }
        } else { //Did not get an action string we expect
            Log.d(TAG, "Action: " + action);
        }
        sendBroadcast(intent); //Broadcast the intent
    }

    public boolean initialize() {
        if (mBluetoothAdapter != null) {
            throw new IllegalStateException("initialized already");
        }
        if (mBluetoothManager == null) { //See if we do not already have the BluetoothManager
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE); //Get the BluetoothManager
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter(); //Ask the BluetoothManager to get the BluetoothAdapter
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        //Success, we have a BluetoothAdapter to control the radio
        return true;
    }

    /**
     * Open a BluetoothGatt connection to a BLE device given its address
     *
     * @param address
     * @return
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) { //Check that we have a Bluetooth adappter and device address
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address."); //Log a warning that something went wrong
            return false; //Failed to connect
        }

        // Previously connected device.  Try to reconnect.
        try {
            if (mBluetoothGatt != null && address.equals(mBluetoothDeviceAddress)) { //See if there was previous connection to the device
                Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
                if (mBluetoothGatt.connect()) { //See if we can connect with the existing BluetoothGatt to connect
                    return true;
                } else {
                    return false;
                }
            }
            //No previous device so get the Bluetooth device by referencing its address
            final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
            if (device == null) {
                //Check whether a device was returned
                Log.w(TAG, "Device not found.  Unable to connect."); //Warn that something went wrong
                return false; //Failed to find the device
            }

            //Directly connect to the device so autoConnect is false
            mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
            Log.d(TAG, "Trying to create a new connection.");
            mBluetoothDeviceAddress = address;
            //Record the address in case we bneed to reconnect with the existing BluetoothGatt
            return true;
        } catch (IllegalArgumentException ex) {
            Log.w(TAG, "Unable to connect. " + ex.getMessage()); //Warn that something went wrong
            return false;
        } catch (NullPointerException ex) {
            //mBluetoothGatt != null mBluetoothDeviceAddress != null
            return false;
        }
    }

    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) { //Check that we have a valid GATT connection
            return null;
        }
        return mBluetoothGatt.getServices(); //Get the list of services
    }

    /**
     * Disconnects an existing connection or cancel a pending connection
     * BluetoothGattCallback.onConnectionStateChange() will get the result
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) { //Check that we have a GATT connection to disconnect
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect(); //Disconnect GATT connection
    }

    /**
     * Request a read of a given BluetoothGattCharacteristic. The Read result is reported asynchronously through the
     * BluetoothGattCallback onCharacteristicRead callback method.
     * For information only. This application uses Indication to receive updated characteristic data, not Read
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) { //Check that we have access to a Bluetooth radio
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic); //Request the BluetoothGatt to Read the characteristic
    }

    /**
     * Write to a given characteristic. The completion of the write is reported asynchronously through the
     * BluetoothGattCallback onCharacteristicWrire callback method.
     *
     * @param characteristic
     */
    public void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) { //Check that we have access to a Bluetooth radio
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        int test = characteristic.getProperties(); //Get the properties of the characteristic
        if ((test & BluetoothGattCharacteristic.PROPERTY_WRITE) == 0 && (test & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) == 0) { //Check that the property is writable
            return;
        }

        if (mBluetoothGatt.writeCharacteristic(characteristic)) { //Request the BluetoothGatt to do the Write
            Log.d(TAG, "writeCharacteristic successful"); //The request was accepted, this does not mean the write completed
        } else {
            Log.d(TAG, "writeCharacteristic failed"); //Write request was not accepted by the BluetoothGatt
        }
    }

    /**
     * Enable notification on a characteristic
     * For information only. This application uses Indication, not Notification
     *
     * @param characteristic
     * @param enabled
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) { //Check that we have a GATT connection
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled); //Enable notification and indication for the characteristic

//        if (UUID_MLDP_DATA_PRIVATE_CHARACTERISTIC.equals(characteristic.getUuid())) {
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID_CHARACTERISTIC_NOTIFICATION_CONFIG); //Get the descripter that enables notification on the server
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE); //Set the value of the descriptor to enable notification
        mBluetoothGatt.writeDescriptor(descriptor); //Write the descriptor
//        }
    }

    /**
     * Enable indication on a characteristic
     */
    public void setCharacteristicIndication(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) { //Check that we have a GATT connection
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled); //Enable notification and indication for the characteristic

        // This is specific to our custom profile
//        if (UUID_MLDP_DATA_PRIVATE_CHARACTERISTIC.equals(characteristic.getUuid())) {
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID_CHARACTERISTIC_NOTIFICATION_CONFIG); //Get the descripter that enables indication on the server
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE); //Set the value of the descriptor to enable indication
        mBluetoothGatt.writeDescriptor(descriptor); //Write the descriptor
//        }
    }

    public void scan(final boolean enable, BluetoothAdapter.LeScanCallback callback) {
        if (enable) {
            boolean x = mBluetoothAdapter.startLeScan(callback);
            Log.d(TAG, "Start scanner");
        } else {
            mBluetoothAdapter.stopLeScan(callback);
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    public void scan(final boolean enable, ScanCallback callback) {
        if (enable) {
            mBluetoothAdapter.getBluetoothLeScanner().startScan(callback);
        } else {
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(callback);
        }
    }

    /**
     * Return this instance of BluetoothService so clients can call its public methods
     */
    public class LocalBinder extends Binder {
        public BluetoothService getService() {
            return BluetoothService.this;
        }
    }
}
