package pl.kayon.driveby.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;

import pl.kayon.driveby.gui.auth.AuthBody;
import pl.kayon.driveby.gui.auth.AuthResponse;
import pl.kayon.driveby.gui.auth.UserInfoResponse;
import pl.kayon.driveby.model.Meter;
import pl.kayon.driveby.model.Route;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.TaskEntity;
import pl.kayon.driveby.model.User;

/**
 * Created by rafalmodrzynski on 19.01.2018.
 */

public class NetworkUtilities {
    public static final String DOMAIN = "bem-iot-api.kayon.pl";
    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static final String HTTP_PUT = "PUT";
    public static final String HTTP_DELETE = "DELETE";
    private static final String TAG = "NetworkUtilities";
    private static final String METHOD_AUTH = "/auth";
    private static final String METHOD_ROUTES = "/routes";
    private static final String METHOD_TASKS = "/tasks";
    private static final String METHOD_ROUTE_DETAILS = "/routes/%d";
    private static final String METHOD_FRAMES = "/frames";

    private static NetworkUtilities instance = new NetworkUtilities();

    public static NetworkUtilities getInstance() {
        return instance;
    }

    public static void getTaskDetails(User user, Task task) throws ResponseException, RequestException {
//        HTTPResult result = call(HTTP_GET, String.format(METHOD_ROUTE_DETAILS, route.getId()), null, user.getToken());
//        Log.d(TAG, "getRouteDetails: " + result.responseBody);
    }

    public static HTTPResult call(String request,
                                  String path,
                                  String body,
                                  String authToken) throws RequestException, ResponseException {
        URL url = null;
        HttpURLConnection conn = null;
        OutputStreamWriter writer = null;
        HTTPResult result = new HTTPResult();
        StringBuffer response = new StringBuffer();
        try {
            url = new URL("https://" + DOMAIN + path);
        } catch (MalformedURLException ex) {
            throw new RequestException("Malformed URL.");
        }
        try {
            // Construct the HttpURLConnection connection object
            try {
                conn = (HttpURLConnection) url.openConnection();
            } catch (MalformedURLException ex) {
                throw new IOException("Cannot connect to Network");
            }
            if (authToken != null) {
                conn.setRequestProperty("Authorization", authToken);
            }
            conn.setUseCaches(false);
            conn.setConnectTimeout(5 * 1000);
            conn.setRequestProperty("User-Agent", "Drive-By-Manager-Android");
            conn.setRequestMethod(request);

            if (request.equals(HTTP_POST) || request.equals(HTTP_PUT)) {    //body.length() > 0
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Length", "" + body.getBytes("UTF-8").length);

                conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                conn.connect();

                try {
                    writer = new OutputStreamWriter(conn.getOutputStream());
                    writer.write(body);
                    writer.flush();
                } catch (IOException e) {
                    throw new RequestException("IOException writing http request.");
                } finally {
                    if (null != writer)
                        writer.close();
                }
            } else if (request.equals(HTTP_GET)) {
                conn.connect();
            } else if (request.equals(HTTP_DELETE)) {
                conn.connect();
            } else {
                throw new RequestException("Invalid HTTP Request.");
            }

            Log.d(TAG, "Responce code " + conn.getResponseCode());
            result.responseCode = conn.getResponseCode();
            InputStreamReader isr;
            if (result.responseCode >= 300) {
                isr = new InputStreamReader(conn.getErrorStream());
            } else {
                isr = new InputStreamReader(conn.getInputStream());
            }
            try (BufferedReader reader = new BufferedReader(isr)) {
                String line;
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
                Log.d(TAG, "Responce body " + response);
                result.responseBody = response.toString();
            } catch (IOException e) {
                throw new ResponseException("Error reading response", conn.getResponseCode(), "");
            }

            // Retrieve response if using {@link #HTTP_GET} or {@link# HTTP_POST}
            if (result.responseCode != HttpURLConnection.HTTP_OK &&
                    result.responseCode != HttpURLConnection.HTTP_CREATED) {
                if (result.responseCode == 422) { //422 Unprocessable Entity

                }
                String message = "Error";
                try {
                    JSONObject obj = new JSONObject(result.responseBody);
                    message = obj.getString("message");
                } catch (JSONException e) {
                    throw new ResponseException("Invalid response body", result.responseCode, result.responseBody);
                }
                throw new ResponseException(message, result.responseCode, result.responseBody);
            }
        } catch (SSLHandshakeException e) {
            throw new RequestException("SSLHandshakeException");
        } catch (IOException e) {
            throw new RequestException(e.getMessage());
        } finally {
            if (conn != null)
                conn.disconnect();
        }
        return result;
    }

    public AuthResponse authenticate(AuthBody body) throws ResponseException, RequestException {
        Gson gson = new GsonBuilder().create();
        HTTPResult result = call(HTTP_POST, METHOD_AUTH, gson.toJson(body), null);
        if (result.responseCode != HttpsURLConnection.HTTP_OK) {
            throw new RequestException("Wystąpił błąd [Code:" + result.responseCode + " " + result.responseBody);
        }
        return gson.fromJson(result.responseBody, AuthResponse.class);
    }

    public UserInfoResponse authenticate(User user) throws ResponseException, RequestException {
        Gson gson = new GsonBuilder().create();
        HTTPResult result = call(HTTP_GET, METHOD_AUTH, null, user.getToken());
        if (result.responseCode != HttpsURLConnection.HTTP_OK) {
            throw new RequestException("Wystąpił błąd [Code:" + result.responseCode + " " + result.responseBody);
        }
        return gson.fromJson(result.responseBody, UserInfoResponse.class);
    }

    public Collection<Route> getRoutes(User user) throws ResponseException, RequestException {
        HTTPResult result = call(HTTP_GET, METHOD_ROUTES, null, user.getToken());
        Gson gson = new Gson();
        return Arrays.asList(gson.fromJson(result.responseBody, Route[].class));
    }

    public Route getRouteDetails(User user, Route route) throws ResponseException, RequestException {
        HTTPResult result = call(HTTP_GET, String.format(METHOD_ROUTE_DETAILS, route.getId()), null, user.getToken());
        Gson gson = new Gson();
        return gson.fromJson(result.responseBody, Route.class);
    }

    public Collection<Task> getTasks(User user) throws ResponseException, RequestException {
        HTTPResult result = call(HTTP_GET, METHOD_TASKS, null, user.getToken());
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Task.State.class, new Task.StateDeserializer());
        Gson gson = gsonBuilder.create();

        return Arrays.asList(gson.fromJson(result.responseBody, Task[].class));
    }

    public void setTaskResult(User user, TaskEntity te) throws ResponseException, RequestException {
        JsonObject e = new JsonObject();
        e.addProperty("meter_id", te.getMeter().getId());
        e.addProperty("task_id", te.getTask().getId());
        e.addProperty("data", te.getResult());
        e.addProperty("rssi", te.getRssi());
        e.addProperty("read_at", TaskEntity.dateAsString(te.getDate()));
        String body = new Gson().toJson(e);

        HTTPResult result = call(HTTP_POST, METHOD_FRAMES, body, user.getToken());
    }

    public void getTaskResults(User user, Task task) throws ResponseException, RequestException {

    }

    public void updateTask(User user, Task task) throws ResponseException, RequestException {
        JsonObject e = new JsonObject();
        e.addProperty("state", task.getState().ordinal());
        if (task.getEnded() != null) {
            e.addProperty("ended", Task.dateAsString(task.getEnded()));
        }
        String body = new Gson().toJson(e);
        HTTPResult result = call(HTTP_PUT, METHOD_TASKS+"/"+task.getId(), body, user.getToken());

    }

    public static class NetworkUtilitiesMock extends NetworkUtilities {

        private final static Random RANDOM = new Random(System.currentTimeMillis());
        private static long ID = 0;
        public boolean authenticateError = false;
        public long authExpTime = System.currentTimeMillis();
        public int routesCount = 56;
        public int tasksInProgressCount = 6;
        public int tasksDoneCount = 10;
        public int metersMinCount = 200;
        public int metersMaxCount = 500;
        public int changeSuccesResult = 90;
        public int DEFAULT_DELAY = 10;

        public AuthResponse authenticate(AuthBody body) throws ResponseException, RequestException {
            if (authenticateError) {
                throw new RequestException("Wystąpił błąd [Code:" + HttpsURLConnection.HTTP_UNAUTHORIZED + " " + "Mock: Invalid credentials");
            }
            String json = "{'auth_token':'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1MTgxNzgyMTV9.BCkfdkzehIcdg0MX3EpBkjRcY9u3_PljbV_9SW_H1E'}";
            Gson gson = new GsonBuilder().create();
            return gson.fromJson(json, AuthResponse.class);
        }

        private void sleep() {
            try {
                Thread.sleep(DEFAULT_DELAY);
            } catch (InterruptedException e) {
            }
        }

        public UserInfoResponse authenticate(User user) throws ResponseException, RequestException {
            sleep();
            if (authenticateError) {
                throw new RequestException("Wystąpił błąd [Code:" + 422 + " " + "Mock: Signature has expired");
            }
            Gson gson = new GsonBuilder().create();
            String body = String.format("{'login': 'Mock User Name','exp': %d}", authExpTime);
            return gson.fromJson(body, UserInfoResponse.class);
        }

        public Collection<Route> getRoutes(User user) throws ResponseException, RequestException {
            sleep();
            ArrayList<Route> items = new ArrayList<>();
            for (int i = 1; i < routesCount; i++) {
                Route r1 = new Route(++ID);
                r1.setName("Trasa " + ID);
                r1.setDetails("Detale " + ID);
                items.add(r1);
            }
            return items;
        }

        public Route getRouteDetails(User user, Route route) throws ResponseException, RequestException {
            sleep();
            route.getMeters().clear();
            int manCode = 0;
            int manNumber = 0;
            for (int x = 0; x < RANDOM.nextInt(metersMaxCount - metersMinCount) + metersMinCount; x++) {
                Meter m = new Meter();
                m.setId(++ID);
                m.setManufacturerName(Integer.toString(++manCode));
                m.setWmbus(++manNumber);
                m.setAddress1("Ulica " + RANDOM.nextInt(5) + 1);
                m.setAddress2(Integer.toString(RANDOM.nextInt(metersMaxCount) + 1));
                m.setMeterNumber(Integer.toString(RANDOM.nextInt()));
                m.setPpeNumber(Integer.toString(RANDOM.nextInt()));
                route.getMeters().add(m);
            }
            return route;
        }

        public Collection<Task> getTasks(User user) throws ResponseException, RequestException {
            sleep();
            ArrayList<Route> routes = new ArrayList<>(getRoutes(user));
            ArrayList<Task> items = new ArrayList<>();
            for (int i = 0; i < routes.size(); i++) {
                Route r1 = routes.get(i);
                Task task = new Task();
                task.setId(++ID);
                task.setRoute(r1);
                task.setUser(user);
                if (i < tasksInProgressCount) {
                    task.setState(Task.State.IN_PROGRESS);
                } else if (i < tasksInProgressCount + tasksDoneCount) {
                    task.setState(Task.State.DONE);
                    task.setEnded(new Date(System.currentTimeMillis() - RANDOM.nextInt(2000000)));
                } else {
                    task.setState(Task.State.TODO);
                }
                items.add(task);
            }
            return items;
        }

        public void getTaskResults(User user, Task task) throws ResponseException, RequestException {
            getRouteDetails(user, task.getRoute());
            sleep();
            if (task.getState() == Task.State.IN_PROGRESS && task.getEntities().size() == 0) {
                task.getEntities().clear();
                for (Meter m : task.getRoute().getMeters()) {
                    if (RANDOM.nextInt(100) > changeSuccesResult) {
                        continue;
                    }
                    TaskEntity te = new TaskEntity();
                    te.setId(++ID);
                    te.setMeter(m);
                    te.setRssi(RANDOM.nextInt(4));
                    te.setDate(new Date());
                    te.setResult(Long.toString(RANDOM.nextLong()));
                    task.getEntities().add(te);
                }
            }
        }
    }
}
