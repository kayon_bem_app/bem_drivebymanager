package pl.kayon.driveby.utils;

/**
 * Created by rafalmodrzynski on 27.01.2018.
 */

public class RCPException extends Exception {

    public RCPException(String message) {
        super(message);
    }
}
