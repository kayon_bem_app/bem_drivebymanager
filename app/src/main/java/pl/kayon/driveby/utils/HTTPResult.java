package pl.kayon.driveby.utils;

/**
 * Created by rafalmodrzynski on 27.01.2018.
 */

public class HTTPResult {
    /**
     * Code refers to constants of {@link java.net.HttpURLConnection HttpURLConnection}
     */
    public int responseCode;

    public String responseBody;
}
