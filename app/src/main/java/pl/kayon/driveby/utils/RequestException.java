package pl.kayon.driveby.utils;

/**
 * Created by rafalmodrzynski on 27.01.2018.
 */

public class RequestException extends RCPException {

    public RequestException(String message) {
        super(message);
    }
}
