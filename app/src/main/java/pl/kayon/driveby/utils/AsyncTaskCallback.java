package pl.kayon.driveby.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.annotation.StringRes;
import android.widget.Toast;

import pl.kayon.driveby.R;

public interface AsyncTaskCallback {

    public void onPreExecute();

    public void onRequestException(RequestException ex);

    public void onResponseException(ResponseException ex);

    public void onResult(Boolean b);

    public void onCancel();

    public static class ActivityDefault implements AsyncTaskCallback {
        protected final Activity activity;
        private final ProgressDialog progressDialog;
        boolean showMessageOnResult = false;

        public ActivityDefault(Activity activity, @StringRes int progressTextRes) {
            this.activity = activity;
            this.progressDialog = new ProgressDialog(activity);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(activity.getString(progressTextRes));
        }

        @Override
        public void onPreExecute() {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.show();
                }
            });
        }

        public void onFail(final String message) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (message != null) {
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();
                }
            });
        }


        @Override
        public void onRequestException(RequestException ex) {
            onFail(ex.getMessage());
        }

        @Override
        public void onResponseException(ResponseException ex) {
            onFail(ex.getMessage());
        }

        @Override
        public void onResult(Boolean b) {
            if (b && showMessageOnResult) {
                onFail(activity.getString(R.string.msg_saved));
            } else {
                onFail(null);
            }
        }

        @Override
        public void onCancel() {
            onFail(activity.getString(R.string.msg_canceled));
        }
    }
}
