package pl.kayon.driveby.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Created by rafalmodrzynski on 23.01.2018.
 */

public class Preferences {
    private static final String USER_TOKEN = "user_token";
    private static final String USER_TOKEN_EXPIRES = "user_token_expires";
    private static final String DEVICE_SERIAL = "device_serial";
    private static final String LOGGER_ENABLED = "logger_enabled";
    private final SharedPreferences preferences;

    public Preferences(Context context) {
        preferences = context.getSharedPreferences("pl.kayon.driveby", Context.MODE_PRIVATE);
    }

    public String getUserToken() {
        return preferences.getString(USER_TOKEN, null);
    }

    public void setUserToken(String userToken) {
        SharedPreferences.Editor editor  = preferences.edit();
        if (userToken == null) {
            editor.remove(USER_TOKEN);
        } else {
            editor.putString(USER_TOKEN, userToken);
        }
        editor.commit();
    }

    public void setDeviceSerial(String serial) {
        SharedPreferences.Editor editor  = preferences.edit();
        if (serial == null) {
            editor.remove(DEVICE_SERIAL);
        } else {
            editor.putString(DEVICE_SERIAL, serial);
        }
        editor.commit();
    }

    public String getDeviceSerial() {
        return preferences.getString(DEVICE_SERIAL, "");
    }

    public long getUserTokenExpires() {
        return preferences.getLong(USER_TOKEN_EXPIRES, 0);
    }

    public void setUserTokenExpires(long tokenExpires) {
        SharedPreferences.Editor editor  = preferences.edit();
        if (tokenExpires == 0) {
            editor.remove(USER_TOKEN_EXPIRES);
        } else {
            editor.putLong(USER_TOKEN_EXPIRES, tokenExpires);
        }
        editor.commit();
    }

    public boolean isLoggerEnabled() {
        return preferences.getBoolean(LOGGER_ENABLED, false);
    }

    public void setLoggerEnabled(boolean enabled) {
        SharedPreferences.Editor editor  = preferences.edit();
        editor.putBoolean(LOGGER_ENABLED, enabled);
        editor.commit();
    }
}
