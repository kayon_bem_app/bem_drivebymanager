package pl.kayon.driveby.utils;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.security.auth.x500.X500Principal;

/**
 * Created by rafalmodrzynski on 21.01.2018.
 */

public class KeystoreManager {

    public static final String TRANSFORMATION = "RSA/ECB/PKCS1Padding";
    public static final String PROVIDER = "AndroidOpenSSL";
    private static final String ALIAS = "pl.kayon.driveby";
    private static final KeystoreManager instance = new KeystoreManager();
    private KeyStore keyStore;
    private KeyPair keyPair;


    public static KeystoreManager getInstance() {
        if (instance.keyPair == null) {
            throw new Error("Not initialized (call 'init' method first)");
        }
        return instance;
    }

    public static void init() throws GeneralSecurityException, IOException {
        instance.checkAndRecreateKeys();
    }

    private void checkAndRecreateKeys() throws GeneralSecurityException, IOException {
        if (keyStore == null) {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
        }
        KeyStore.Entry entry = keyStore.getEntry(ALIAS, null);
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate(ALIAS);

        if (certificate != null && certificate.getNotAfter().before(new Date())) {
            deleteKey();
            certificate = null;
        }
        if (certificate == null) {
            Calendar end = Calendar.getInstance();
            end.add(Calendar.HOUR, 1);
            KeyPairGenerator generator = KeyPairGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");
            generator.initialize(
                    new KeyGenParameterSpec.Builder(ALIAS, KeyProperties.PURPOSE_SIGN)
                            .setCertificateSubject(new X500Principal("CN=Kayon DriveBy, O=Android Authority"))
                            .setCertificateSerialNumber(BigInteger.valueOf(8834634132412l))
                            .setCertificateNotAfter(end.getTime())
                            .setDigests(KeyProperties.DIGEST_SHA256)
                            .setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PSS)
                            .build());

            keyPair = generator.generateKeyPair();
        } else {
            PrivateKey privateKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
            PublicKey publicKey = certificate.getPublicKey();
            keyPair = new KeyPair(publicKey, privateKey);
        }
    }

    public void deleteKey() throws KeyStoreException {
        keyStore.deleteEntry(ALIAS);
    }

    public String encryptString(String initialText) throws GeneralSecurityException, IOException {
        if (initialText.isEmpty()) {
            return "";
        }
        Cipher input = Cipher.getInstance(TRANSFORMATION, PROVIDER);
        input.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, input)) {
            cipherOutputStream.write(initialText.getBytes("UTF-8"));
            return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }

    /**
     * @param cipherText Base64 text
     */
    public String decryptString(String cipherText) throws GeneralSecurityException, IOException {
        Cipher output = Cipher.getInstance(TRANSFORMATION, PROVIDER);
        output.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());

        try {
            CipherInputStream cipherInputStream = new CipherInputStream(
                    new ByteArrayInputStream(Base64.decode(cipherText, Base64.DEFAULT)), output);
            ArrayList<Byte> values = new ArrayList<>();
            int nextByte;
            while ((nextByte = cipherInputStream.read()) != -1) {
                values.add((byte) nextByte);
            }

            byte[] bytes = new byte[values.size()];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = values.get(i).byteValue();
            }

            return new String(bytes, 0, bytes.length, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }
}
