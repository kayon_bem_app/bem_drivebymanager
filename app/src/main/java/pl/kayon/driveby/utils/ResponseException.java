package pl.kayon.driveby.utils;

/**
 * Created by rafalmodrzynski on 27.01.2018.
 */

public class ResponseException extends RCPException {

    private int responseCode;
    private String responseBody;

    public ResponseException(String message) {
        super(message);
    }

    public ResponseException(final String message,
                                    int responseCode,
                                    String responseBody) {
        super(message);
        this.responseCode = responseCode;
        this.responseBody = responseBody;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseBody() {
        return responseBody;
    }
}
