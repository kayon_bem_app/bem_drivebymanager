package pl.kayon.driveby.utils;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rafalmodrzynski on 25.01.2018.
 */

public class FileLogger {

    private static final SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static FileLogger instance = new FileLogger();
    private boolean enabled;
    private File folder;
    private String status;
    private String status2;
    private File logFile;

    public FileLogger() {
        init();
    }

    public static FileLogger getInstance() {
        return instance;
    }

    private void init() {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            status = "Brak karty SD";
            return;
        }
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            status = "Karta SD jest w trybie 'ReadOnly'";
            return;
        }

        folder = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), "Kayon");
        setEnabled(folder.exists());
        if (!folder.exists()) {
            status = "Nie można utworzyć katalogu " + folder.getAbsolutePath();
            return;
        }
        status = "Logi zapisywane w katalogu " + folder.getAbsolutePath();
    }

    public void newLogFile() {
        String fileName = sf.format(new Date());
        logFile = new File(folder, fileName);
    }

    public void log(String text) {
        if (!enabled) {
            status2 = "Logowanie wyłączone";
            return;
        }
        if (logFile == null) {
            newLogFile();
        }
        try (FileOutputStream outputStream = new FileOutputStream(logFile, true)) {
            outputStream.write(text.getBytes());
            status2 = "Logi prawidłowo zapisane do pliku " + logFile.getAbsolutePath();
        } catch (Exception e) {
            if (e.getMessage() != null) {
                status2 = e.getMessage();
            } else {
                status2 = e.getClass().getSimpleName();
            }
        }
    }

    public String getStatus() {
        return status;
    }

    public String getStatus2() {
        return status2;
    }

    private void setEnabled(boolean enabled) {
        this.enabled = enabled;
        if (enabled) {
            newLogFile();
        }
    }
}
