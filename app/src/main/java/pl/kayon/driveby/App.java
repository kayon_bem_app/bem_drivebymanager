package pl.kayon.driveby;

import android.app.Application;
import android.os.Environment;
import android.os.StrictMode;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.database.Database;

import java.io.File;
import java.util.Date;

import pl.kayon.driveby.model.DaoMaster;
import pl.kayon.driveby.model.DaoSession;
import pl.kayon.driveby.model.TaskEntityLocalCache;
import pl.kayon.driveby.utils.FileLogger;
import pl.kayon.driveby.utils.Preferences;

public class App extends Application {

    /** A flag to show how easily you can switch from standard SQLite to the encrypted SQLCipher. */
    public static final boolean ENCRYPTED = false;

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        Preferences preferences = new Preferences(getApplicationContext());
        FileLogger.getInstance().log(new Date().toString());

//        File path = new File(Environment.getExternalStorageDirectory(), "cache-db");
//        path.getParentFile().mkdirs();
//
//        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, path.getAbsolutePath(), null);
//        Database db = helper.getWritableDb();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, ENCRYPTED ? "cache-db-encrypted" : "cache-db");
        Database db = ENCRYPTED ? helper.getEncryptedWritableDb("super-secret") : helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here

        }
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
