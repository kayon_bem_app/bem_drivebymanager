package pl.kayon.driveby.mbus;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by rafalmodrzynski on 10.01.2018.
 */

public class FrameBuilder {

    private static final FrameBuilder instance = new FrameBuilder();
    private Buffer buffer;


    private FrameBuilder() {
    }

    public static FrameBuilder getInstance() {
        return instance;
    }

    public ArrayList<KayonFrame> append(byte[] data) throws FrameBuilderException {
        try {
            ArrayList<KayonFrame> frames = new ArrayList<>();
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            boolean created = false;
            while (in.available() > 0) {
                byte z = (byte) in.read();
                if (buffer == null) {
                    buffer = Buffer.create(z);
                }
                if (buffer == null) {
                    continue;
                } else {
                    if (!buffer.append(z)) {
                        buffer = null;
                        continue;
                    } else {
                        if (buffer.isComplete()) {
                            frames.add(buffer.getFrame());
                            buffer = null;
                        }
                    }
                }
            }
            return frames;
        } catch (IOException ex) {
            buffer = null;
            throw new FrameBuilderException(ex, data);
        }
    }

    private static abstract class Buffer {
        protected final int headerLen;
        private ByteBuffer buffer = ByteBuffer.allocate(512);

        public Buffer(int headerLen) {
            this.headerLen = headerLen;
        }

        public static Buffer create(byte z) {
            int i = z & 0xFF;
            switch (i) {
                case 0x55:
                    return new MeterBuffer();
                case 0xAA:
                    return new BatteryCollect();
                default:
                    return null;
            }
        }

        public abstract KayonFrame getFrame() throws IOException;

        protected abstract int getPayloadSize();

        public final boolean append(byte z) {
            if (isComplete()) {
                return false;
            }
            if (!isHeaderComplete()) {
                return appendHeader(z);
            } else {
                return appendPayload(z);
            }
        }

        protected byte[] getBytes() {
            byte[] d = new byte[buffer.position()];
            buffer.position(0);
            buffer.get(d);
            return d;
        }

        protected final void put(byte z) {
            buffer.put(z);
        }

        protected final int collected() {
            return buffer.position();
        }

        protected final boolean isHeaderComplete() {
            return collected() >= headerLen;
        }

        protected abstract boolean appendHeader(byte z);

        protected boolean appendPayload(byte z) {
            buffer.put(z);
            return true;
        }

        public final boolean isComplete() {
            int collected = buffer.position();
            return collected == headerLen + getPayloadSize();
        }

    }

    private static class MeterBuffer extends Buffer {
        private static final int HEADER_LEN = 6;
        private int rssi;
        private int len = 0;


        public MeterBuffer() {
            super(HEADER_LEN);
        }

        protected boolean appendHeader(byte z) {
            int i = z & 0xFF;
            switch (collected()) {
                case 0: //0x55
                case 1: //0x55
                case 2: //0x55
                case 3: //0x55
                    if (0x55 != i) {
                        return false;
                    } else {
                        put(z);
                    }
                    break;
                case 4: //RSSI
                    put(z);
                    break;
                case 5: //len
                    put(z);
                    len = z & 0xFF;
                    break;
            }
            return true;
        }

        @Override
        protected int getPayloadSize() {
            return len;
        }

        @Override
        public MeterFrame getFrame() throws IOException {
            return new MeterFrame(getBytes(), 0);
        }
    }

    private static class BatteryCollect extends Buffer {
        private static final int HEADER_LEN = 4;
        private static final int PAYLOAD_SIZE = 5;

        public BatteryCollect() {
            super(HEADER_LEN);
        }

        @Override
        protected int getPayloadSize() {
            return PAYLOAD_SIZE;
        }

        protected boolean appendHeader(byte z) {
            int i = z & 0xFF;
            switch (collected()) {
                case 0: //0xAA
                case 1: //0xAA
                case 2: //0xAA
                case 3: //0xAA
                    if (0xAA != i) {
                        return false;
                    } else {
                        put(z);
                    }
                    break;
            }
            return true;
        }

        @Override
        public KayonFrame getFrame() throws IOException {
            return new StatusFrame(getBytes(), 0);
        }
    }
}

