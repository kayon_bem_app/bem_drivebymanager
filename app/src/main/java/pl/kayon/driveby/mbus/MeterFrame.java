package pl.kayon.driveby.mbus;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import pl.kayon.driveby.model.Meter;

/**
 * Created by rafalmodrzynski on 12.01.2018.
 */

public class MeterFrame extends KayonFrame {

    private final int rssi;
    private TFormatBFrame mbusData;

    public MeterFrame(byte[] buffer, int offset) throws InvalidDataException, IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(buffer);
        in.skip(offset);
        for (int i = 0; i < 4; i++) {
            if (in.read() != 0x55) {
                throw new InvalidDataException("Invalid header");
            }
        }
        rssi = in.read() & 0xFF;
        mbusData = new TFormatBFrame(in, 0);
    }

    public int getRssi() {
        return rssi;
    }

    public TFormatBFrame getMbusData() {
        return mbusData;
    }

    public Meter getMeter() {
        Meter meter = new Meter();
        meter.setRssi(rssi);
        meter.setType(mbusData.getDeviceType());
        meter.setWmbus(mbusData.getWmbus().longValue());
        meter.setManufacturerName(mbusData.getManufactureCode());
        meter.setVersion(mbusData.getVersion());
        meter.setLastFrame(mbusData);
        return meter;
    }
}
