package pl.kayon.driveby.mbus;

import java.io.IOException;

/**
 * Created by rafalmodrzynski on 10.01.2018.
 */

public class AmiFrame {

    public static class DateTime extends AmiFrame {
        // Data i czas
        public static final int DIF = 0x06;
        public static final int VIF = 0x6D;

//        public DateTime create(InputStream in) throws InvalidData, IOException {
//            if (in.read() != DIF) {
//                throw new InvalidData();
//            }
//            if (in.read() != VIF) {
//                throw new InvalidData();
//            }
//        }
    }

    public static class SerialNumber extends AmiFrame {
        // Numer seryjny licznika
    }

    public static class EnergyCollected extends AmiFrame {
        // Energia czynna pobrana
    }

    public static class EnergyDevoted extends AmiFrame {
        // Energia czynna oddana

    }

    public static class MaxValuePowerCollected extends AmiFrame {
        // Max wartość pobranej mocy czynnej

    }

    public static class TempPowerCollected extends AmiFrame {
        // Moc czynna pobrana – wartość chwilowa
    }

    public static class TempPowerDevoted extends AmiFrame {
        // Moc czynna oddana – wartość chwilowa
    }

    public static class TempVoltage1 extends AmiFrame {
        // Napięcie chwilowe – faza 1
    }

    public static class TempVoltage2 extends AmiFrame {
        // Napięcie chwilowe – faza 2
    }

    public static class TempVoltage3 extends AmiFrame {
        // Napięcie chwilowe – faza 3
    }

    public static class PowerAboveLimit extends AmiFrame {
        // Moc powyżej progu ograniczenia mocy
    }

    public static class Message extends AmiFrame {
        // Komunikat dla użytkownika

    }

    public static class InvalidData extends IOException {

    }
}
