package pl.kayon.driveby.mbus;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by rafalmodrzynski on 12.01.2018.
 */

public class StatusFrame extends KayonFrame {

//0xAA, 0xAA, 0xAA, 0xAA - nagłówek ramki
    private double batteryVoltage;
    private int[] versionI;
    private String version;
    private int crc;

    public StatusFrame(byte[] buffer, int offset) throws InvalidDataException, IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(buffer);
        in.skip(offset);
        for (int i = 0; i < 4; i++) {
            if (in.read() != 0xAA) {
                throw new InvalidDataException("Invalid header");
            }
        }
        int v = in.read() & 0xFF; //V_bat - napięcie baterii z rozdzielczością 100mV (np wartość 0x27 to 3,9V)
        batteryVoltage = v * 0.1;
        versionI = new int[3];
        for (int i=0; i<versionI.length; i++) { //Wer_mj, Wer_mi, Wer_rel - oznaczenie wersji fw
            versionI[i] = in.read() & 0xFF;
        }
        version = String.format("%d.%d.%d", versionI[0], versionI[1], versionI[2]);
        crc = in.read() & 0xFF; //CRC - suma modulo 256 z wszystkich bajtów ramki
    }

    public double getBatteryVoltage() {
        return batteryVoltage;
    }

    public int[] getVersionI() {
        return versionI;
    }

    public String getVersion() {
        return version;
    }
}
