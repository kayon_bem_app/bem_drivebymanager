package pl.kayon.driveby.mbus;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by rafalmodrzynski on 12.01.2018.
 */

public class TFormatBFrame {

    public static final int SEND_NO_REPLY = 0x44;
    private final String manufactureCode;
    private final Bcd wmbus;
    private final int version;
    private final DeviceType deviceType;
    private byte[] variableData;

    public TFormatBFrame(ByteArrayInputStream in, int offset) throws InvalidDataException, IOException {
        in.skip(offset);
        //first block
        in.mark(in.available());
        int len = (in.read() & 0xFF) + 1 ; //+1 bajt długości
        switch (in.read() & 0xFF) { //C-Field
            case SEND_NO_REPLY:
                manufactureCode = readManufactureCode(in);
                wmbus = readWmbus(in);
                version = in.read() & 0xFF;
                deviceType = DeviceType.getInstance(in.read() & 0xFF);
                break;
            default:
                throw new InvalidDataException("Unsupported data");
        }
        in.reset();
        variableData = new byte[len];
        in.read(variableData);

    }

    public TFormatBFrame(byte[] buffer, int offset) throws InvalidDataException, IOException {
        this(new ByteArrayInputStream(buffer), offset);
    }

    public String readManufactureCode(ByteArrayInputStream is) throws IOException {
        int bytes = 2;
        byte[] idArray = new byte[bytes];
        int actual = is.read(idArray);
        if (bytes != actual) {
            throw new IOException("Failed to read BCD data. Data missing.");
        }

        int mc = ((idArray[1] & 0xFF) << 8) + (idArray[0] & 0xFF);
        long base = mc;
        int z1 = (int) (base / 1024l);
        base -= 1024 * z1;
        int z2 = (int) (base / 32l);
        base -= 32 * z2;
        int z3 = (int) base;

        return String.format("%c%c%c", z1 + 64, z2 + 64, z3 + 64);
    }

    public Bcd readWmbus(ByteArrayInputStream is) throws IOException {
        int msgSize = 4;
        byte[] bytes = new byte[msgSize];
        int actual = is.read(bytes);
        if (msgSize != actual) {
            throw new IOException("Failed to read BCD data. Data missing.");
        }
        return new Bcd(bytes);
    }

    public String getManufactureCode() {
        return manufactureCode;
    }

    public Bcd getWmbus() {
        return wmbus;
    }

    public int getVersion() {
        return version;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public byte[] getVariableData() {
        return variableData;
    }

    @Override
    public String toString() {
        return "ManufactureCode: " + manufactureCode + " wmbus: " + wmbus.longValue() + " DeviceType: " + deviceType;
    }
}
