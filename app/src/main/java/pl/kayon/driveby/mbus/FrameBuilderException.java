package pl.kayon.driveby.mbus;

/**
 * Created by rafalmodrzynski on 01.02.2018.
 */

public class FrameBuilderException extends Exception {
    private byte[] data;

    public FrameBuilderException(String message, byte[] data) {
        super(message);
        this.data = data;
    }

    public FrameBuilderException(String message, Throwable cause, byte[] data) {
        super(message, cause);
        this.data = data;
    }

    public FrameBuilderException(Throwable cause, byte[] data) {
        super(cause);
        this.data = data;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + "[" + dataToString() + "]";
    }

    @Override
    public String getLocalizedMessage() {
        return super.getMessage() + "[" + dataToString() + "]";
    }

    private String dataToString() {
        StringBuilder buf = new StringBuilder();
        for (byte b: data) {
            buf.append(String.format("%02X ", b));
        }
        return buf.toString();
    }
}
