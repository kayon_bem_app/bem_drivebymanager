package pl.kayon.driveby.mbus;

import java.io.IOException;

/**
 * Created by rafalmodrzynski on 12.01.2018.
 */

public class InvalidDataException extends IOException {
    public InvalidDataException() {
    }

    public InvalidDataException(String message) {
        super(message);
    }

    public InvalidDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidDataException(Throwable cause) {
        super(cause);
    }
}
