package pl.kayon.driveby.mbus;

/**
 * Created by rafalmodrzynski on 11.01.2018.
 */

public class Bcd extends Number {

    private static final long serialVersionUID = 880515601567539930L;
    private final byte[] value;

    public Bcd(byte[] bcdBytes) {
        this.value = bcdBytes;
    }

    public byte[] getBytes() {
        return value;
    }

    @Override
    public String toString() {
        byte[] bytes = new byte[value.length * 2];
        int c = 0;

        if ((value[value.length - 1] & 0xf0) == 0xf0) {
            bytes[c++] = 0x2d;
        } else {
            bytes[c++] = (byte) (((value[value.length - 1] >> 4) & 0x0f) + 48);
        }

        bytes[c++] = (byte) ((value[value.length - 1] & 0x0f) + 48);

        for (int i = value.length - 2; i >= 0; i--) {
            bytes[c++] = (byte) (((value[i] >> 4) & 0x0f) + 48);
            bytes[c++] = (byte) ((value[i] & 0x0f) + 48);
        }

        return new String(bytes);
    }

    @Override
    public double doubleValue() {
        return longValue();
    }

    @Override
    public float floatValue() {
        return longValue();
    }

    @Override
    public int intValue() {
        return (int) longValue();
    }

    @Override
    public long longValue() {
        long result = 0l;
        long factor = 1l;

        for (int i = 0; i < (value.length - 1); i++) {
            result += (value[i] & 0x0f) * factor;
            factor = factor * 10l;
            result += ((value[i] >> 4) & 0x0f) * factor;
            factor = factor * 10l;
        }

        result += (value[value.length - 1] & 0x0f) * factor;
        factor = factor * 10l;

        if ((value[value.length - 1] & 0xf0) == 0xf0) {
            result = result * -1;
        } else {
            result += ((value[value.length - 1] >> 4) & 0x0f) * factor;
        }

        return result;
    }

}

