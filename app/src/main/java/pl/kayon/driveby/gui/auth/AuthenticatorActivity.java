package pl.kayon.driveby.gui.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.text.MessageFormat;

import pl.kayon.driveby.App;
import pl.kayon.driveby.R;
import pl.kayon.driveby.gui.main.MainActivity;
import pl.kayon.driveby.model.User;
import pl.kayon.driveby.utils.Preferences;
import pl.kayon.driveby.utils.RequestException;
import pl.kayon.driveby.utils.ResponseException;

/**
 * Created by rafalmodrzynski on 14.01.2018.
 */
public class AuthenticatorActivity extends AppCompatActivity {

    private static final String TAG = AuthenticatorActivity.class.getSimpleName();
    TextView loginText;
    EditText passwordText;
    Button loginButton;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginText = (TextView) findViewById(R.id.input_login);
        passwordText = (EditText) findViewById(R.id.input_password);
        loginButton = (Button) findViewById(R.id.btn_login);
        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed(getString(R.string.msg_invalid_credential));
            return;
        }

        loginButton.setEnabled(false);

        progressDialog = new ProgressDialog(AuthenticatorActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.lbl_authenticating));
        progressDialog.show();

        String login = loginText.getText().toString();
        String password = passwordText.getText().toString();
        final String userName = login;
        final Preferences preferences = new Preferences(this);
        AuthBody body = new AuthBody(login, password);
        new AuthentitationTask(body, new AuthentitationTask.Callback() {
            @Override
            public void onAuthenticationResult(AuthResponse authResponse) {
                if (authResponse == null) {
                    return;
                }
                User u = User.loginDefault(authResponse.getToken(), 0);
                u.loadDevice(AuthenticatorActivity.this);
                new UserDataTask(new UserDataTask.Callback() {
                    @Override
                    public void onRequestException(RequestException ex) {
                        onLoginFailed(MessageFormat.format(getString(R.string.msg_data_download_failed), ex.getMessage()));
                    }

                    @Override
                    public void onResponseException(ResponseException ex) {
                        onLoginFailed(MessageFormat.format(getString(R.string.msg_data_download_failed), ex.getMessage()));
                    }

                    @Override
                    public void onResult(Boolean b) {
                        if (b == null) {
                            return;
                        }
                        onLoginSuccess();

                        Intent intent = new Intent(AuthenticatorActivity.this, MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onCancel() {
                        onLoginFailed("Canceled");
                    }
                }, (App)getApplication()).execute(User.getUser());
            }

            @Override
            public void onAuthenticationCancel() {
                onLoginFailed("Canceled");
            }

            @Override
            public void onRequestException(RequestException ex) {
                onLoginFailed(MessageFormat.format(getString(R.string.msg_login_failed), ex.getMessage()));
            }

            @Override
            public void onResponseException(ResponseException ex) {
                if (ex.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    onLoginFailed(getString(R.string.msg_invalid_credential));
                } else {
                    onLoginFailed(MessageFormat.format(getString(R.string.msg_login_failed), ex.getMessage()));
                }
            }
        }).execute();
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        User user = User.getUser();
        final Preferences preferences = new Preferences(this);
        preferences.setUserToken(user.getToken());
        preferences.setUserTokenExpires(user.getTokenExpires());
        loginButton.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
        onLoginFailed(getString(R.string.msg_login_failed));
    }

    public void onLoginFailed(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (message != null) {
                    Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
                }
                loginButton.setEnabled(true);
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        });
    }

    public boolean validate() {
        boolean valid = true;

        String login = loginText.getText().toString();
        String password = passwordText.getText().toString();
        if (login.equals("1")) {
            return true;
        }
        if (login.isEmpty()) {
            // brak zalozen dla loginu - android.util.Patterns
            // loginText.setError("enter a valid email address");
            valid = false;
        } else {
            loginText.setError(null);
        }

        if (password.isEmpty()) {
            // brak zalozen dla hasla
            // password.length() < 4 || password.length() > 10
            //passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

}