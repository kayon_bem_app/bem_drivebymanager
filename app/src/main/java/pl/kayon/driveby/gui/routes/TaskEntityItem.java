package pl.kayon.driveby.gui.routes;

import android.view.View;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFlexible;
import pl.kayon.driveby.R;
import pl.kayon.driveby.gui.flexibleAdapter.HeaderItem;
import pl.kayon.driveby.gui.flexibleAdapter.SimpleItem;
import pl.kayon.driveby.model.TaskEntity;

public class TaskEntityItem extends SimpleItem<TaskEntityItem.TaskEntityItemViewHolder> {

    private static final SimpleDateFormat sf = new SimpleDateFormat("MM-dd HH:mm:ss");
    private final TaskEntity entity;

    private TaskEntityItem(TaskEntity entity, boolean noIcon) {
        super(entity.getMeter().getId().toString());
        this.entity = entity;
        this.title = entity.getMeter().getAddress2();
        if (entity.getDate() != null) {
            this.subtitle = sf.format(entity.getDate());
        }
        this.flipViewHidden = noIcon;
        setDraggable(false);
        setSwipeable(false);
    }

    public TaskEntityItem(TaskEntity entity, Header header, boolean noIcon) {
        this(entity, noIcon);
        this.header = header;
    }

    @Override
    public Header getHeader() {
        return (Header) header;
    }

    public TaskEntity getEntity() {
        return entity;
    }

    @Override
    public void onViewAttached(FlexibleAdapter<IFlexible> adapter, TaskEntityItemViewHolder holder, int position) {
        super.onViewAttached(adapter, holder, position);
        TaskEntityItem te = (TaskEntityItem)adapter.getItem(position);
        holder.getFlipView().setRearImage(R.drawable.ic_meter_done);
        holder.getFlipView().setFrontImage(R.drawable.ic_device_offline);
        holder.getFlipView().flip(te.getEntity().getResult() != null);
//        StringBuilder b = new StringBuilder();
//        if (item.getEntity().getMeter().getPpeNumber() != null) {
//            b.append(String.format(getResources().getString(R.string.ppe_number_format),
//                                   item.getEntity().getMeter().getPpeNumber()));
//        }
//        if (item.getEntity().getMeter().getMeterNumber() != null) {
//            if (b.length() > 0) {
//                b.append(" ");
//            }
//            b.append(String.format(getResources().getString(R.string.meter_number_format),
//                                   item.getEntity().getMeter().getMeterNumber()));
//        }
//        if (b.length() > 0) {
//            holder.getTitleView().setText(te.getTitle() + b.toString());
//        }
    }

    public TaskEntityItemViewHolder createViewHolder(View view, FlexibleAdapter<IFlexible> adapter) {
        return new TaskEntityItemViewHolder(view, adapter);
    }

    static final class TaskEntityItemViewHolder  extends SimpleItem.SimpleViewHolder {
        TaskEntityItem item;
        TaskEntityItemViewHolder (View view, FlexibleAdapter<IFlexible> adapter) {
            super(view, adapter);
        }

        @Override
        public void onClick(View view) {
            final TaskEntityItem item = ((TaskResultsAdapter) mAdapter).getItem(getAdapterPosition());
            StringBuilder b = new StringBuilder();
            String ppeNumber = item.getEntity().getMeter().getPpeNumber();
            if (ppeNumber != null && ppeNumber.length() > 0) {
                b.append(String.format(mContext.getResources().getString(R.string.ppe_number_format),
                                       ppeNumber));
            }
            String meterNumber = item.getEntity().getMeter().getMeterNumber();
            if (meterNumber != null && meterNumber.length() > 0) {
                if (b.length() > 0) {
                    b.append(" ");
                }
                b.append(String.format(mContext.getResources().getString(R.string.meter_number_format),
                                       meterNumber));
            }
            if (b.length() > 0) {
                Toast.makeText(mContext, b.toString(), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mContext, mContext.getString(R.string.no_details), Toast.LENGTH_LONG).show();
            }
            super.onClick(view);
        }
    }

    public static class Header extends HeaderItem {
        public Header(String id) {
            super(id);
            this.subtitleNoElements = R.string.header_subtitle_no_meters;
            this.subtitleElementsFormat = R.string.header_subtitle_meters_format;
        }
    }
}
