package pl.kayon.driveby.gui.routes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.Date;

import pl.kayon.driveby.R;
import pl.kayon.driveby.model.MetersInRange;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.TaskEntity;
import pl.kayon.driveby.model.User;
import pl.kayon.driveby.utils.AsyncTaskCallback;
import pl.kayon.driveby.utils.NetworkUtilities;
import pl.kayon.driveby.utils.RequestException;
import pl.kayon.driveby.utils.ResponseException;

/**
 * Created by rafalmodrzynski on 26.01.2018.
 */

public class TaskResultsTask extends AsyncTask<Task, Void, Boolean> {

    private final AsyncTaskCallback callback;

    public TaskResultsTask(AsyncTaskCallback callback) {
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        callback.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Task... tasks) {
        try {
            User user = User.getUser();
            for (Task t : tasks) {
                for (TaskEntity te : t.getEntities()) {
                    NetworkUtilities.getInstance().setTaskResult(user, te);
                }
                NetworkUtilities.getInstance().updateTask(user, t);
            }
            return true;
        } catch (RequestException ex) {
            callback.onRequestException(ex);
        } catch (ResponseException ex) {
            callback.onResponseException(ex);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean b) {
        callback.onResult(b);
    }

    @Override
    protected void onCancelled() {
        callback.onCancel();
    }

}
