package pl.kayon.driveby.gui.auth;

import android.os.AsyncTask;

import org.greenrobot.greendao.query.WhereCondition;

import java.util.Arrays;
import java.util.List;

import pl.kayon.driveby.App;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.TaskEntity;
import pl.kayon.driveby.model.TaskEntityLocalCache;
import pl.kayon.driveby.model.TaskEntityLocalCacheDao;
import pl.kayon.driveby.model.User;
import pl.kayon.driveby.utils.NetworkUtilities;
import pl.kayon.driveby.utils.RequestException;
import pl.kayon.driveby.utils.ResponseException;

/**
 * Created by rafalmodrzynski on 26.01.2018.
 */

public class UserDataTask extends AsyncTask<User, Void, Boolean> {

    private final App app;
    private final Callback callback;

    public UserDataTask(Callback callback, App app) {
        this.callback = callback;
        this.app = app;
    }

    @Override
    protected Boolean doInBackground(User... users) {
        try {
            for (User u : users) {
                UserInfoResponse userInfoResponse = NetworkUtilities.getInstance().authenticate(u);
                u.setName(userInfoResponse.getLogin());
                u.setTokenExpires(userInfoResponse.getTokenExpires());
                u.getTasks().clear();
                u.getTasks().addAll(NetworkUtilities.getInstance().getTasks(u));
                for (Task task : u.getTasks()) {
                    if (task.getState() != Task.State.DONE) {
                        //przeszukanie cache
                        TaskEntityLocalCacheDao dao = app.getDaoSession().getTaskEntityLocalCacheDao();
                        List<TaskEntityLocalCache> cache = dao.queryBuilder()
                                .where(TaskEntityLocalCacheDao.Properties.TaskId.eq(Long.valueOf(task.getId())))
                                .where(TaskEntityLocalCacheDao.Properties.Result.isNotNull())
                                .orderAsc(TaskEntityLocalCacheDao.Properties.MeterId)
                                .list();
                        if (cache.size() > 0) {
                            task.setState(Task.State.IN_PROGRESS);
                        }
                        for (TaskEntityLocalCache c : cache) {
                            TaskEntity te = c.toTaskEntity(task);
                            task.getEntities().add(te);
                        }
                    }
                }
            }
            return true;
        } catch (RequestException ex) {
            callback.onRequestException(ex);
        } catch (ResponseException ex) {
            callback.onResponseException(ex);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Boolean b) {
        callback.onResult(b);
    }

    @Override
    protected void onCancelled() {
        callback.onCancel();
    }

    public static interface Callback {

        public void onRequestException(RequestException ex);

        public void onResponseException(ResponseException ex);

        public void onResult(Boolean b);

        public void onCancel();
    }
}
