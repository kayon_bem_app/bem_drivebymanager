package pl.kayon.driveby.gui.devicesInRange;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;

import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFlexible;
import eu.davidea.flexibleadapter.utils.DrawableUtils;
import pl.kayon.driveby.R;
import pl.kayon.driveby.gui.flexibleAdapter.HeaderItem;
import pl.kayon.driveby.gui.flexibleAdapter.SimpleItem;

public class DeviceInRangeItem extends SimpleItem<DeviceInRangeItem.DeviceInRangeItemViewHolder> {

    private final DriveByDevice device;
    protected long life = System.currentTimeMillis();

    public DeviceInRangeItem(DriveByDevice device) {
        super(device.getAddress());
        this.device = device.clone();
        this.title = device.getName();
        this.subtitle = device.getAddress();
        this.flipViewHidden = false;
        setDraggable(false);
        setSwipeable(false);
    }

    public DeviceInRangeItem(DriveByDevice device, HeaderItem header) {
        this(device);
        this.header = header;
    }

    @Override
    public void bindViewHolder(FlexibleAdapter<IFlexible> adapter, DeviceInRangeItemViewHolder holder, int position, List<Object> payloads) {
        super.bindViewHolder(adapter, holder, position, payloads);
        //podmiana ikonki
        Context context = holder.itemView.getContext();
//        Drawable drawable = DrawableUtils.getSelectableBackgroundCompat(
//                Color.WHITE, Color.parseColor("#dddddd"), // Same color of divider
//                DrawableUtils.getColorControlHighlight(context));
//        DrawableUtils.setBackgroundCompat(holder.itemView, drawable);

        Drawable drawable = DrawableUtils.getSelectableBackgroundCompat(
                Color.WHITE,  // normal background
                Color.WHITE,  // pressed background
                context.getResources().getColor(R.color.list_choice_pressed_bg_light)); // ripple color

        DrawableUtils.setBackgroundCompat(holder.getFrontView(), drawable);

        holder.getFlipView().setFrontImage(R.drawable.ic_device_online);

        holder.getTitleView().setTextColor(context.getResources().getColor(R.color.text));
        holder.getSubtitleView().setTextColor(context.getResources().getColor(R.color.text));
        if (device.getVersion() != null) {
            String t = String.format("%s v. %s ⚡︎%1.1fV", device.getName(), device.getVersion(), device.getBatteryVoltage());
            holder.getTitleView().setText(t);
        }
        if (adapter.isSelected(position)) {
            if (device.isOnline()) {
                holder.getFlipView().setChildBackgroundColor(1,
                                                             context.getResources().getColor(R.color.device_online_paired));
                holder.getFlipView().setRearImage(R.drawable.ic_device_paired);
            } else {
                holder.getFlipView().setChildBackgroundColor(1,
                                                             context.getResources().getColor(R.color.device_offline_paired));
                holder.getFlipView().setRearImage(R.drawable.ic_device_offline);
            }
        } else {
            holder.getFlipView().setChildBackgroundColor(1,
                                                         context.getResources().getColor(R.color.device_online_paired));
            holder.getFlipView().setRearImage(R.drawable.ic_device_paired);
        }
    }

    public boolean isOld() {
        return System.currentTimeMillis() - life > 15000;
    }


    public DeviceInRangeItem.DeviceInRangeItemViewHolder createViewHolder(View view, FlexibleAdapter<IFlexible> adapter) {
        return new DeviceInRangeItem.DeviceInRangeItemViewHolder(view, adapter);
    }

    public DriveByDevice getDevice() {
        return device;
    }

    static final class DeviceInRangeItemViewHolder extends SimpleItem.SimpleViewHolder {

        DeviceInRangeItemViewHolder(View view, FlexibleAdapter<IFlexible> adapter) {
            super(view, adapter);
        }

        @Override
        public boolean onLongClick(View view) {
            return super.onLongClick(view);
        }
    }
}