package pl.kayon.driveby.gui.flexibleAdapter;

import android.support.annotation.DrawableRes;

public class PageInfo {
    private String toolbarTitle;
    private String title;
    private String subtitle;
    private @DrawableRes int icon;
    private @DrawableRes int background;

    public void setToolbarTitle(String toolbarTitle) {
        this.toolbarTitle = toolbarTitle;
    }

    public String getToolbarTitle() {
        return toolbarTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }
}
