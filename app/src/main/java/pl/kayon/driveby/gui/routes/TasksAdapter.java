package pl.kayon.driveby.gui.routes;

import java.util.List;

import pl.kayon.driveby.gui.flexibleAdapter.AbstractAdapter;

public class TasksAdapter extends AbstractAdapter<TaskItem> {

    public TasksAdapter(List<TaskItem> items, Object listeners) {
        super(items, listeners, false);
        mItemLongClickListener = null;
    }

}
