package pl.kayon.driveby.gui.auth;

import com.google.gson.annotations.SerializedName;

import pl.kayon.driveby.utils.HTTPResult;

/**
 * Created by rafalmodrzynski on 04.02.2018.
 */

public class AuthResponse {

    @SerializedName("auth_token")
    private String token;

    public String getToken() {
        return token;
    }

}
