package pl.kayon.driveby.gui.flexibleAdapter;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import eu.davidea.flexibleadapter.SelectableAdapter;

public interface OnFragmentInteractionListener {

    void onFragmentChange(AbstractFragment fragment, SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView,
                          @SelectableAdapter.Mode int mode);

    void initSearchView(final Menu menu);

}