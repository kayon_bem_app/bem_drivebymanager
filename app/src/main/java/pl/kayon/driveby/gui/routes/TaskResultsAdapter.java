package pl.kayon.driveby.gui.routes;

import java.util.List;

import pl.kayon.driveby.gui.flexibleAdapter.AbstractAdapter;

public class TaskResultsAdapter extends AbstractAdapter<TaskEntityItem> {

    public TaskResultsAdapter(List<TaskEntityItem> items, Object listeners) {
        super(items, listeners, false);
        mItemLongClickListener = null;
    }

}
