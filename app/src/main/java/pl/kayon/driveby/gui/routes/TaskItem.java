package pl.kayon.driveby.gui.routes;

import android.content.Context;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFlexible;
import pl.kayon.driveby.R;
import pl.kayon.driveby.gui.flexibleAdapter.HeaderItem;
import pl.kayon.driveby.gui.flexibleAdapter.SimpleItem;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.TaskEntity;

public class TaskItem extends SimpleItem<TaskItem.TaskItemViewHolder> {

    public static final SimpleDateFormat sf = new SimpleDateFormat("dd MMM yyyy HH:mm");
    private final Task task;

    private TaskItem(Task task) {
        super(Long.toString(task.getId()));
        this.task = task;
        this.title = task.getRoute().getName();
        this.subtitle = task.getRoute().getDetails();
        setDraggable(false);
        setSwipeable(false);
    }

    public TaskItem(Task task, Header header) {
        this(task);
        this.header = header;
    }

    public Task getTask() {
        return task;
    }

    @Override
    public void bindViewHolder(FlexibleAdapter<IFlexible> adapter, TaskItemViewHolder holder, int position, List<Object> payloads) {
        Context c = holder.itemView.getContext();
        if (task.getState() == Task.State.DONE && task.getEnded() != null) {
            this.subtitle = String.format(c.getString(R.string.date_ended_task_format), sf.format(task.getEnded()));
        } else if (task.getState() == Task.State.IN_PROGRESS) {
            TaskEntity te = task.getEntities().getLast();
            if (te != null && te.getDate() != null) {
                if (task.getRoute().getMeters().size() > 0) {
                    double fProc = Double.valueOf(task.getEntities().size()) / Double.valueOf(task.getRoute().getMeters().size());
                    int proc = (int) (fProc * 100);
                    this.subtitle = String.format(c.getString(R.string.date_update_proc_task_format), sf.format(te.getDate()), proc);
                } else {
                    this.subtitle = String.format(c.getString(R.string.date_update_task_format), sf.format(te.getDate()));
                }
            }
        } else {
            this.subtitle = task.getRoute().getDetails();
        }
        super.bindViewHolder(adapter, holder, position, payloads);
    }

    public TaskItemViewHolder createViewHolder(View view, FlexibleAdapter<IFlexible> adapter) {
        return new TaskItemViewHolder(view, adapter);
    }

    static final class TaskItemViewHolder extends SimpleItem.SimpleViewHolder {

        TaskItemViewHolder(View view, FlexibleAdapter<IFlexible> adapter) {
            super(view, adapter);
        }

    }

    public static class Header extends HeaderItem {
        public Header(String id) {
            super(id);
            this.subtitleNoElements = R.string.header_subtitle_no_routes;
            this.subtitleElementsFormat = R.string.header_subtitle_routes_format;
        }
    }
}
