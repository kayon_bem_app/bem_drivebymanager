package pl.kayon.driveby.gui.devicesInRange;

import android.os.Handler;
import android.os.Looper;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.items.IFlexible;
import pl.kayon.driveby.gui.flexibleAdapter.AbstractAdapter;
import pl.kayon.driveby.model.User;

public class DevicesInRangeAdapter extends AbstractAdapter<DeviceInRangeItem> {

    //    private TreeSet<DriveByDevice> devices;
//    private DriveByDevice[] array;
//    private DriveByDevice pairedDevice;
//    private Comparator<DriveByDevice> uniqueComparator = new Comparator<DriveByDevice>() {
//        @Override
//        public int compare(DriveByDevice o1, DriveByDevice o2) {
//            return o1.getAddress().compareTo(o2.getAddress());
//        }
//    };
//    private Comparator<DriveByDevice> sortComparator = new Comparator<DriveByDevice>() {
//        @Override
//        public int compare(DriveByDevice o1, DriveByDevice o2) {
//            int c = Integer.compare(o1.getStatus(), o2.getStatus());
//            if (c != 0) {
//                return c;
//            }
//            return o1.getAddress().compareTo(o2.getAddress());
//        }
//    };


    public DevicesInRangeAdapter(List<DeviceInRangeItem> items, Object listeners) {
        super(items, listeners, false);
        mItemLongClickListener = null;
        setMode(Mode.SINGLE);
    }

    public void resetLifeSelected() {
        List<Integer> selections = getSelectedPositions();
        for (Integer i : selections) {
            try {
                DeviceInRangeItem item = (DeviceInRangeItem) getItem(i);
                item.life = System.currentTimeMillis();
            } catch (ClassCastException ex) {
            }
        }
    }

    public void removeOldest() {
        synchronized (this) {
            final ArrayList<DeviceInRangeItem> toRemove = new ArrayList<>();
            final ArrayList<DeviceInRangeItem> toUpdate = new ArrayList<>();
            for (IFlexible f : getCurrentItems()) {
                if (f instanceof DeviceInRangeItem) {
                    boolean selected = isSelected(getGlobalPositionOf(f));
                    DeviceInRangeItem d = (DeviceInRangeItem) f;
                    if (!selected && (d.isOld() || !d.getDevice().isOnline())) {
                        toRemove.add(d);
                    } else if (selected && !User.getUser().getDevice().equalsAllFields(d.getDevice())) {
                        DeviceInRangeItem item = new DeviceInRangeItem(User.getUser().getDevice());
                        toUpdate.add(item);
                    }
                }
            }
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    try {
                        for (DeviceInRangeItem d : toUpdate) {
                            updateItem(d);
                        }
                        for (DeviceInRangeItem d : toRemove) {
                            removeItem(getGlobalPositionOf(d));
                        }
                    } catch (IllegalStateException ex) {
                        Crashlytics.logException(ex);
                    }
                }
            });
        }
    }

    public void update(DeviceInRangeItem newItem) {
        synchronized (this) {
            int position = getGlobalPositionOf(newItem);
            if (position == -1) {
                addItem(newItem);
            } else {
                updateItem(newItem);
            }
        }
    }

    //    public DevicesInRangeAdapter(Context context) {
//        Preferences preferences = new Preferences(context);
//        pairedDevice = new DriveByDevice();
//        pairedDevice.setOnline(false);
//        pairedDevice.setAddress(preferences.getDeviceSerial());
////        pairedDevice.setStatus(DriveByDevice.PAIRED_OFFLINE);
//        devices = new TreeSet<>(uniqueComparator);
//        addDevice(pairedDevice);
//
////        DriveByDevice fake = new DriveByDevice();
////        fake.setStatus(DriveByDevice.ONLINE);
////        fake.setAddress("00:11:22:33:44:55");
////        addDevice(fake);
//    }
//
//    public void setPair(DriveByDevice device) {
//        if (!pairedDevice.equalsAddress(device)) {
//            pairedDevice = device;
//            notifyDataSetChanged();
//        }
//    }
//
//    public void addDevice(DriveByDevice device) {
//        DriveByDevice old = null; //devices.ceiling(device);
//        for (DriveByDevice d : devices) {
//            if (d.equalsAddress(device)) {
//                old = d;
//                old.setOnline(true);
//                notifyDataSetChanged();
//                break;
//            }
//        }
//        if (old == null) {
//            devices.add(device);
//            TreeSet<DriveByDevice> sorted = new TreeSet<>(sortComparator);
//            sorted.addAll(devices);
//            array = sorted.toArray(new DriveByDevice[0]);
//            notifyDataSetChanged();
//        }
//    }
//
//    public DriveByDevice getDevice(int position) {
//        return array[position];
//    }
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.list_device_item, parent, false);
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(final ViewHolder holder, int position) {
//        DriveByDevice device = array[position];
//        holder.setDevice(device, device.equalsAddress(pairedDevice));
//    }
//
//    @Override
//    public int getItemCount() {
//        return devices.size();
//    }
//
//    public static class ViewHolder extends RecyclerView.ViewHolder {
//        private final View view;
//        private final TextView idView;
//        private final TextView contentView;
//        private final ImageView imageView;
//        private DriveByDevice device;
//
//        private ViewHolder(View view) {
//            super(view);
//            this.view = view;
//            idView = (TextView) view.findViewById(R.id.text_device_id);
//            idView.setText(null);
//            contentView = (TextView) view.findViewById(R.id.text_device_name);
//            contentView.setText(null);
//            imageView = (ImageView) view.findViewById(R.id.image_device);
//            imageView.setImageDrawable(null);
//        }
//
//        private void setDevice(DriveByDevice device, boolean paired) {
//            this.device = device;
//            idView.setText(device.getName());
//            contentView.setText(device.getAddress());
//            if (paired) {
//                if (device.isOnline()) {
//                    imageView.setImageResource(R.drawable.ic_list_connect_ok);
//                } else {
//                    imageView.setImageResource(R.drawable.ic_list_connect_error);
//                }
//            } else {
//                imageView.setImageDrawable(null);
//            }
//        }
//
//        @Override
//        public String toString() {
//            return super.toString() + " '" + contentView.getText() + "'";
//        }
//    }
}
