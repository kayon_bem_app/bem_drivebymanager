package pl.kayon.driveby.gui.routes;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.Random;

import at.grabner.circleprogress.CircleProgressView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import pl.kayon.driveby.App;
import pl.kayon.driveby.R;
import pl.kayon.driveby.gui.flexibleAdapter.AbstractFragment;
import pl.kayon.driveby.gui.flexibleAdapter.PageInfo;
import pl.kayon.driveby.mbus.DeviceType;
import pl.kayon.driveby.mbus.FrameBuilder;
import pl.kayon.driveby.mbus.FrameBuilderException;
import pl.kayon.driveby.mbus.KayonFrame;
import pl.kayon.driveby.mbus.MeterFrame;
import pl.kayon.driveby.mbus.TFormatBFrame;
import pl.kayon.driveby.model.DaoSession;
import pl.kayon.driveby.model.Meter;
import pl.kayon.driveby.model.MetersInRange;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.TaskEntity;
import pl.kayon.driveby.model.TaskEntityLocalCache;
import pl.kayon.driveby.model.TaskEntityLocalCacheDao;
import pl.kayon.driveby.model.User;

public class TaskDetailsFragment extends AbstractFragment implements MetersInRange.Listener {

    public static final String TAG = TaskDetailsFragment.class.getSimpleName();
    private static final String ARG_TASK_ID = "taskID";
    private CircleProgressView circleView;
    private Task task;
    private Handler handler = new Handler(Looper.getMainLooper());
    private TextView counterErrorTextView;
    private TextView counterReadTextView;
    private TextView counterUnreadTextView;
    private Thread thread;
    private TaskEntityLocalCacheDao dao;

    public TaskDetailsFragment() {
    }

    public static TaskDetailsFragment newInstance(Task task) {
        TaskDetailsFragment fragment = new TaskDetailsFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_TASK_ID, task.getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Long taskId = getArguments().getLong(ARG_TASK_ID);
            task = User.getUser().getTasks().findById(taskId);
        }
        DaoSession daoSession = ((App) getActivity().getApplication()).getDaoSession();
        dao = daoSession.getTaskEntityLocalCacheDao();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route_details, container, false);

        circleView = (CircleProgressView) view.findViewById(R.id.circle_view);
        circleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof FlexibleAdapter.OnItemClickListener) {
                    //((FlexibleAdapter.OnItemClickListener) getActivity()).onItemClick(circleView, TaskResultsFragment.Filter.ALL.ordinal());
                }
            }
        });

        counterReadTextView = (TextView) view.findViewById(R.id.counter_read);
        counterReadTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof FlexibleAdapter.OnItemClickListener) {
                    ((FlexibleAdapter.OnItemClickListener) getActivity()).onItemClick(counterReadTextView, TaskResultsFragment.Filter.READ.ordinal());
                }
            }
        });
        counterUnreadTextView = (TextView) view.findViewById(R.id.counter_unread);
        counterUnreadTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof FlexibleAdapter.OnItemClickListener) {
                    ((FlexibleAdapter.OnItemClickListener) getActivity()).onItemClick(counterReadTextView, TaskResultsFragment.Filter.UNREAD.ordinal());
                }
            }
        });

        if (task.getState() == Task.State.TODO) {
            restartTask();
        } else if (task.getState() == Task.State.IN_PROGRESS) {
            setUpProgress();
            simulate();
        } else {
            setUpProgress();
        }

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_task_restart:
                restartTask();
                return true;
            case R.id.action_task_end:
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializePageInfo();
        MetersInRange.getInstance().setListener(this);
        mListener.onFragmentChange(this, null, null, 0);
    }


    @Override
    public void onDestroyView() {
        if (thread != null) {
            thread.interrupt();
        }
        super.onDestroyView();
        MetersInRange.getInstance().removeListener(this);
    }

    private void initializePageInfo() {
        pageInfo = new PageInfo();
        pageInfo.setToolbarTitle(getContext().getResources().getString(R.string.title_route_details));
        pageInfo.setTitle(task.getRoute().getName());
        pageInfo.setBackground(R.drawable.background_info_blue);
        pageInfo.setIcon(R.drawable.ic_info_route);
    }

    @Override
    public int getOptionsMenuResId() {
        return R.menu.route_details;
    }

    private void setUpProgress() {
        int all = task.getRoute().getMeters().size();
        int read = task.getEntities().size();
        int unread = all - read;

        circleView.setMaxValue(all);
        circleView.setUnit(getString(R.string.route_in_progress));
        circleView.setUnitVisible(true);
        counterReadTextView.setText(Integer.toString(read));
        counterUnreadTextView.setText(Integer.toString(unread));
        circleView.setValue(read);
        circleView.setText(Integer.toString(read) + " / " + all);
    }

    private void restartTask() {
        setUpProgress();

        if (thread != null) {
            thread.interrupt();
        }
        simulate();
    }

    private void simulate() {
        thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);

                    int max = task.getRoute().getMeters().size();
                    Random rnd = new Random(System.currentTimeMillis());
                    int unread = 0;

                    while (true) {
                        for (Meter m : task.getRoute().getMeters()) {
                            if (isInterrupted()) {
                                return;
                            }
                            m.setType(DeviceType.ELECTRICITY_METER);
                            if (rnd.nextInt(100) < 90) {
                                m.setRssi(rnd.nextInt(4));
                                byte[] data = new byte[30];
                                rnd.nextBytes(data);
                                data[1] = TFormatBFrame.SEND_NO_REPLY;
                                try {
                                    m.setLastFrame(new TFormatBFrame(data, 0));
                                } catch (Exception ex) {
                                }
                                MetersInRange.getInstance().add(m);
                            } else {
                                unread++;
                            }


                            sleep(rnd.nextInt(400) + 100);
                        }
                    }
                } catch (InterruptedException e) {
                    return;
                }
            }
        };
//        thread.start();

        thread = new Thread() {
            @Override
            public void run() {
                String x = "55555555209e4401064801008802027a00109005d12e639f0a653d9ec4" +
                        "cefcc53d15d2793f40de91ec6f2dc9ea85e8eb01412caf7813d6" +
                        "3dbc2f06b627a84a543fd08a64673a70e38f9398d909e6c07ba3" +
                        "1c7eb52538bc047427911ffcf54b7b670bc64f187236e9cf1e42" +
                        "3b9a9491a741b1f2c8552657819225394ff9abf8c49eaf5aaba4" +
                        "bd3f56b9ac464c84ff95f9f77f3006b56cd856d57db5c0796ed5218eaff761";
                byte[] b = new byte[x.length() / 2];
                for (int i = 0; i < b.length; i++) {
                    String hex = x.substring(i * 2, i * 2 + 2);
                    b[i] = (byte) Integer.parseInt(hex, 16);
                }
                try {
                    for (KayonFrame kf : FrameBuilder.getInstance().append(b)) {
                        if (kf instanceof MeterFrame) {
                            MeterFrame f = (MeterFrame)kf;
                            Meter meter = f.getMeter();
                            MetersInRange.getInstance().add(meter);
                            Log.d(TAG, String.format("RSSI %d %s", f.getRssi(), f.getMbusData().toString()));
                        }
                    }
                } catch (FrameBuilderException e) {
                    e.printStackTrace();
                }
            }
        };
//        thread.start();
    }
    @Override
    public void wasChanged() {
        Meter meter = null;


        for (Meter m : MetersInRange.getInstance().allWithClear()) {
            //wyszukiwanie
            for (Meter mm : task.getRoute().getMeters()) {
                if (m.getUniqueNumber().equals(mm.getUniqueNumber())) {
                    meter = mm;
                    break;
                }
            }
            if (meter == null) {
                continue;
            }
            TaskEntity entity = new TaskEntity();
            entity.setTask(task);
            entity.setMeter(meter);
            entity.setDate(new Date());
            entity.setRssi(m.getRssi());
            StringBuilder buf = new StringBuilder();
            entity.setResult(Base64.encodeToString(m.getLastFrame().getVariableData(), Base64.NO_WRAP));
            TaskEntityLocalCache cache = new TaskEntityLocalCache(entity);
            long l = dao.insertOrReplace(cache);
            task.getEntities().add(entity);
        }
        refresh();
    }

    private void refresh() {
        final int max = task.getRoute().getMeters().size();
        final int finalRead = task.getEntities().size();
        final int finalUnread = max - finalRead;
        handler.postAtFrontOfQueue(new Runnable() {
            @Override
            public void run() {
                counterReadTextView.setText(Integer.toString(finalRead));
                counterUnreadTextView.setText(Integer.toString(finalUnread));
                circleView.setValue(finalRead);
                circleView.setText(Integer.toString(finalRead) + " / " + max);
                Log.d(TAG, "Read loop.... value : " + finalRead);
            }
        });

    }

    public Task getTask() {
        return task;
    }
}
