package pl.kayon.driveby.gui.main;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import pl.kayon.driveby.bluetooth.BluetoothService;
import pl.kayon.driveby.model.User;

public class MainServiceConnection implements ServiceConnection,  User.Listener {
    public static final String TAG = MainServiceConnection.class.getSimpleName();

    private BluetoothService bluetoothService;

    void onDestroy() {
        //Not bound to a service
        bluetoothService = null;
    }

    public MainServiceConnection() {
        User.getUser().addListener(this);
    }

    void reconnect() {
        if (bluetoothService != null) { //Check that service is running
            Log.d(TAG, "Disconnect");
            bluetoothService.disconnect();
            if (!User.getUser().getDevice().isVoid()) {
                final boolean result = bluetoothService.connect(User.getUser().getDevice().getAddress()); //Ask the service to connect to the GATT server hosted on the Bluetooth LE device
                Log.d(TAG, "Connect request result = " + result);
            }

        }

    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service) {    //Service connects
        bluetoothService = ((BluetoothService.LocalBinder) service).getService(); //Get a link to the service
        if (!bluetoothService.initialize()) { //See if the service did not initialize properly
            Log.e(TAG, "Unable to initialize Bluetooth");
            //finish(); //End the application
        }
        if (User.getUser().getDevice() != null) {
            bluetoothService.connect(User.getUser().getDevice().getAddress()); //Connects to the device selected and passed to us by the DeviceScanActivity
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) { //Service disconnects
        bluetoothService = null; //Not bound to a service
    }

    public BluetoothService getBluetoothService() {
        return bluetoothService;
    }

    @Override
    //User.Listener
    public void onChanged(String property) {
        if (property.equals(User.PROP_DEVICE)) {
            reconnect();
        }
    }
}
