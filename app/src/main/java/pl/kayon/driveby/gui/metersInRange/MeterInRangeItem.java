package pl.kayon.driveby.gui.metersInRange;

import android.view.View;

import java.util.Locale;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFlexible;
import pl.kayon.driveby.gui.flexibleAdapter.HeaderItem;
import pl.kayon.driveby.gui.flexibleAdapter.SimpleItem;
import pl.kayon.driveby.model.Meter;

public class MeterInRangeItem extends SimpleItem<MeterInRangeItem.MeterInRangeItemViewHolder> {

    private final Meter meter;

    public MeterInRangeItem(Meter meter) {
        super(meter.getUniqueNumber());
        this.meter = meter;
        this.title = String.format(Locale.getDefault(), "SN: %d Code:%s v:%d",
                                   meter.getWmbus(),
                                   meter.getManufacturerName(),
                                   meter.getVersion());
        this.subtitle = String.format(Locale.getDefault(), "RSSI: -%d dB Type: %s Wiek: %d sek",
                                      meter.getRssi(),
                                      meter.getType().toString(),
                                      (System.currentTimeMillis() - meter.getCreatedAt()) / 1000);
        setDraggable(false);
        setSwipeable(false);
    }

    public MeterInRangeItem(Meter meter, HeaderItem header) {
        this(meter);
        this.header = header;
    }

    public Meter getMeter() {
        return meter;
    }

    @Override
    public void onViewAttached(FlexibleAdapter<IFlexible> adapter, MeterInRangeItemViewHolder holder, int position) {
        super.onViewAttached(adapter, holder, position);
        holder.getTitleView().setTextSize(12);
        holder.getSubtitleView().setTextSize(12);
    }

    public MeterInRangeItem.MeterInRangeItemViewHolder createViewHolder(View view, FlexibleAdapter<IFlexible> adapter) {
        return new MeterInRangeItem.MeterInRangeItemViewHolder(view, adapter);
    }

    static final class MeterInRangeItemViewHolder extends SimpleItem.SimpleViewHolder {

        MeterInRangeItemViewHolder(View view, FlexibleAdapter<IFlexible> adapter) {
            super(view, adapter);
        }

    }
}
