package pl.kayon.driveby.gui.routes;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.TreeSet;

import eu.davidea.fastscroller.FastScroller;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.SelectableAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flexibleadapter.items.IFlexible;
import eu.davidea.flipview.FlipView;
import pl.kayon.driveby.App;
import pl.kayon.driveby.R;
import pl.kayon.driveby.gui.flexibleAdapter.AbstractFragment;
import pl.kayon.driveby.gui.flexibleAdapter.PageInfo;
import pl.kayon.driveby.gui.main.MainActivity;
import pl.kayon.driveby.mbus.TFormatBFrame;
import pl.kayon.driveby.model.DaoSession;
import pl.kayon.driveby.model.Meter;
import pl.kayon.driveby.model.MetersInRange;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.TaskEntity;
import pl.kayon.driveby.model.TaskEntityLocalCache;
import pl.kayon.driveby.model.TaskEntityLocalCacheDao;
import pl.kayon.driveby.model.User;

public class TaskResultsFragment extends AbstractFragment implements MetersInRange.Listener {

    public static final String TAG = TaskResultsFragment.class.getSimpleName();
    private static final String ARG_TASK_ID = "taskID";
    private static final String ARG_FILTER = "filter";
    private SwipeRefreshLayout swipeRefreshLayout;
    private TaskResultsAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private Task task;
    private Filter filter;
    private TaskEntityLocalCacheDao dao;

    public static TaskResultsFragment newInstance(Task task, Filter filter) {
        TaskResultsFragment fragment = new TaskResultsFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_TASK_ID, task.getId());
        args.putInt(ARG_FILTER, filter.ordinal());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_task_results, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Long taskId = getArguments().getLong(ARG_TASK_ID);
            task = User.getUser().getTasks().findById(taskId);
            filter = Filter.values()[getArguments().getInt(ARG_FILTER)];
        }
        DaoSession daoSession = ((App) getActivity().getApplication()).getDaoSession();
        dao = daoSession.getTaskEntityLocalCacheDao();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FlipView.resetLayoutAnimationDelay(true, 1000L);

        initializeRecyclerView(savedInstanceState);
        initializePageInfo();

        MetersInRange.getInstance().setListener(this);
        mListener.onFragmentChange(this,
                                   swipeRefreshLayout,
                                   mRecyclerView,
                                   SelectableAdapter.Mode.IDLE);

        FlipView.stopLayoutAnimation();
    }

    private void initializeRecyclerView(Bundle savedInstanceState) {
        FlexibleAdapter.useTag("TaskResultsFragment");
        HashMap<String, TaskEntityItem.Header> headers = new HashMap<>();
        HashMap<String, TreeSet<TaskEntityItem>> sorted = new HashMap<>();

        Comparator<TaskEntityItem> itemsComparator = new Comparator<TaskEntityItem>() {
            @Override
            public int compare(TaskEntityItem o1, TaskEntityItem o2) {
                int c = o1.getEntity().getMeter().getAddress2().compareTo(o2.getEntity().getMeter().getAddress2());
                if (c != 0) {
                    return c;
                }
                return o1.getId().compareTo(o2.getId());
            }
        };
        TreeSet<Meter> unreadMetes = new TreeSet<>(new Comparator<Meter>() {
            @Override
            public int compare(Meter o1, Meter o2) {
                return Long.compare(o1.getId(), o2.getId());
            }
        });
        unreadMetes.addAll(task.getRoute().getMeters());
        boolean noIcon = filter != Filter.ALL;
        for (TaskEntity te : task.getEntities()) {
            unreadMetes.remove(te.getMeter());
            String headerName = te.getMeter().getGroupName();
            TaskEntityItem.Header header = headers.get(headerName);
            if (header == null) {
                header = new TaskEntityItem.Header(headerName);
                header.setTitle(headerName);
                headers.put(headerName, header);
            }
            TreeSet<TaskEntityItem> items = sorted.get(headerName);
            if (items == null) {
                items = new TreeSet<>(itemsComparator);
                sorted.put(headerName, items);
            }
            if (filter == Filter.ALL || filter == Filter.READ) {
                items.add(new TaskEntityItem(te, header, noIcon));
            }
        }
        for (Meter m : unreadMetes) {
            String headerName = m.getGroupName();
            TaskEntityItem.Header header = headers.get(headerName);
            if (header == null) {
                header = new TaskEntityItem.Header(headerName);
                header.setTitle(headerName);
                headers.put(headerName, header);
            }
            TreeSet<TaskEntityItem> items = sorted.get(headerName);
            if (items == null) {
                items = new TreeSet<>(itemsComparator);
                sorted.put(headerName, items);
            }
            TaskEntity te = new TaskEntity();
            te.setTask(task);
            te.setMeter(m);
            te.setResult(null);
            if (filter == Filter.ALL || filter == Filter.UNREAD) {
                items.add(new TaskEntityItem(te, header, noIcon));
            }
        }
        ArrayList<TaskEntityItem> items = new ArrayList<>();
        for (Collection<TaskEntityItem> col : sorted.values()) {
            for (TaskEntityItem i : col) {
                items.add(i);
            }
        }
        mAdapter = new TaskResultsAdapter(items, getActivity());

        mAdapter.setNotifyMoveOfFilteredItems(true)
                .setAnimationOnForwardScrolling(true);

        mRecyclerView = getView().findViewById(R.id.recycler_view);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(createNewLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true); //Size of RV will not change
        // NOTE: Use default item animator 'canReuseUpdatedViewHolder()' will return true if
        // a Payload is provided. FlexibleAdapter is actually sending Payloads onItemChange.
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new FlexibleItemDecoration(getContext())
//                                                .addItemViewType(R.layout.recycler_header_item, 8, 8, 8, 8)
                                                .addItemViewType(R.layout.recycler_simple_item, 0, 0, 0, 1)
                                                .withSectionGapOffset(10)
                                                .withEdge(true));


        // Add FastScroll to the RecyclerView, after the Adapter has been attached the RecyclerView!!!
        FastScroller fastScroller = getView().findViewById(R.id.fast_scroller);
        fastScroller.addOnScrollStateChangeListener((MainActivity) getActivity());
        mAdapter.setFastScroller(fastScroller);

        // New empty views handling, to set after FastScroller
//        EmptyViewHelper.create(mAdapter,
//                               getView().findViewById(R.id.empty_view),
//                               getView().findViewById(R.id.filter_view),
//                               (EmptyViewHelper.OnEmptyViewListener) getActivity()); // Optional!!

        // More settings
        mAdapter.setLongPressDragEnabled(false)
                .setHandleDragEnabled(false)
                .setSwipeEnabled(false)
                .setStickyHeaderElevation(0)
                .setUnlinkAllItemsOnRemoveHeaders(true)
                // Show Headers at startUp, 1st call, correctly executed, no warning log message!
                .setDisplayHeadersAtStartUp(true)
                .setStickyHeaders(true);
        // Simulate developer 2nd call mistake, now it's safe, not executed, no warning log message!
        //.setDisplayHeadersAtStartUp(true)
        // Simulate developer 3rd call mistake, still safe, not executed, warning log message displayed!
        //.showAllHeaders();

        swipeRefreshLayout = getView().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setEnabled(true);

        // Add 2 Scrollable Headers and 1 Footer
//        mAdapter.addUserLearnedSelection(savedInstanceState == null);
//        mAdapter.addScrollableHeaderWithDelay(new ScrollableUseCaseItem(
//                getString(R.string.headers_sections_use_case_title),
//                getString(R.string.headers_sections_use_case_description)), 900L, false);
//        mAdapter.addScrollableFooter();
    }

    private void initializePageInfo() {
        pageInfo = new PageInfo();
        pageInfo.setTitle(task.getRoute().getName());
        if (filter == Filter.UNREAD) {
            pageInfo.setBackground(R.drawable.background_info_gray);
            pageInfo.setIcon(R.drawable.ic_info_counter_unread);
        } else {
            pageInfo.setBackground(R.drawable.background_info_green);
            pageInfo.setIcon(R.drawable.ic_info_counter_read);
        }

    }

    @Override
    public int getOptionsMenuResId() {
        return R.menu.menu_empty;
    }

    @Override
    public void onResume() {
        super.onResume();

        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);

                    int max = task.getRoute().getMeters().size();
                    Random rnd = new Random(System.currentTimeMillis());
                    int unread = 0;

                    while (true) {
                        for (Meter m : task.getRoute().getMeters()) {
                            if (isInterrupted()) {
                                return;
                            }
                            if (rnd.nextInt(100) < 90) {
                                m.setRssi(rnd.nextInt(4));
                                byte[] data = new byte[30];
                                rnd.nextBytes(data);
                                data[1] = TFormatBFrame.SEND_NO_REPLY;
                                try {
                                    m.setLastFrame(new TFormatBFrame(data, 0));
                                } catch (Exception ex) {
                                }
                                MetersInRange.getInstance().add(m);
                            } else {
                                unread++;
                            }


                            sleep(rnd.nextInt(400) + 100);
                        }
                    }
                } catch (InterruptedException e) {
                    return;
                }
            }
        };
//        .start();
//        userInfoTextView.setText(User.getUser().getName());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void wasChanged() {
        Meter meter = null;


        for (Meter m : MetersInRange.getInstance().allWithClear()) {
            //wyszukiwanie
            for (Meter mm : task.getRoute().getMeters()) {
                if (m.getUniqueNumber().equals(mm.getUniqueNumber())) {
                    meter = mm;
                    break;
                }
            }
            if (meter == null) {
                continue;
            }
            final TaskEntity entity = new TaskEntity();
            entity.setTask(task);
            entity.setMeter(meter);
            entity.setDate(new Date());
            entity.setRssi(m.getRssi());
            StringBuilder buf = new StringBuilder();
            entity.setResult(Base64.encodeToString(m.getLastFrame().getVariableData(), Base64.NO_WRAP));
            TaskEntityLocalCache cache = new TaskEntityLocalCache(entity);
            long l = dao.insertOrReplace(cache);
            task.getEntities().add(entity);

            String itemId = Long.toString(meter.getId());
            if (filter == Filter.READ) {
                String headerName = meter.getGroupName();
                TaskEntityItem.Header hItem = null;
                TaskEntityItem item = null;
                for (IFlexible fItem : mAdapter.getCurrentItems()) {
                    if (hItem == null && fItem instanceof TaskEntityItem.Header) {
                        TaskEntityItem.Header hI = (TaskEntityItem.Header) fItem;
                        if (hI.getId().equals(headerName)) {
                            hItem = hI;
                        }
                    } else if (item == null && fItem instanceof TaskEntityItem) {
                        TaskEntityItem it = (TaskEntityItem) fItem;
                        if (it.getId().equals(itemId)) {
                            item = it;
                        }
                    }
                    if (hItem != null && item != null) {
                        break;
                    }
                }

                if (item != null) {
                    final TaskEntityItem finalItem = item;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            while (true) {
                                try {
                                    TaskEntityItem newItem = new TaskEntityItem(entity, finalItem.getHeader(), true);
                                    mAdapter.updateItem(newItem);
                                    break;
                                } catch (IllegalStateException ex) {
                                    Crashlytics.logException(ex);
                                }
                            }
                        }
                    });
                } else {
                    final int pos;
                    if (hItem == null) {
                        //nie ma ale jest grupa
                        hItem = new TaskEntityItem.Header(headerName);
                        hItem.setTitle(headerName);
                        pos = 0;
                    } else {
                        pos = mAdapter.getGlobalPositionOf(hItem) + 1;
                    }
                    final TaskEntityItem.Header fhItem = hItem;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mAdapter.addItem(pos, new TaskEntityItem(entity, fhItem, true));
                            } catch (IllegalStateException ex) {
                                Crashlytics.logException(ex);
                            }
                        }
                    });
                }
            } else {
                for (IFlexible fItem : mAdapter.getCurrentItems()) {
                    if (fItem instanceof TaskEntityItem) {
                        final TaskEntityItem item = (TaskEntityItem) fItem;
                        if (item.getId().equals(itemId)) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        mAdapter.removeItemWithDelay(item, 0, true);
                                    } catch (IllegalStateException ex) {
                                        Crashlytics.logException(ex);
                                    }
                                }
                            });
                            break;
                        }
                    }
                }
            }
        }
    }

    public static enum Filter {
        ALL, READ, UNREAD;

        public String getLocalizableName(Context context) {
            switch (this) {
                case ALL:
                    return context.getString(R.string.title_route_details);
                case READ:
                    return context.getString(R.string.title_route_details_read);
                case UNREAD:
                    return context.getString(R.string.title_route_details_unread);
                default:
                    return context.getString(R.string.title_route_details);
            }
        }
    }
}
