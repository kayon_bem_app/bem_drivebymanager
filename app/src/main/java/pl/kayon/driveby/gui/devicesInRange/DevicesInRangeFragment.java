package pl.kayon.driveby.gui.devicesInRange;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.fastscroller.FastScroller;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.SelectableAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import pl.kayon.driveby.R;
import pl.kayon.driveby.bluetooth.BluetoothService;
import pl.kayon.driveby.gui.flexibleAdapter.AbstractFragment;
import pl.kayon.driveby.gui.flexibleAdapter.PageInfo;
import pl.kayon.driveby.gui.main.MainActivity;
import pl.kayon.driveby.model.User;
import pl.kayon.driveby.utils.Preferences;
import pl.kayon.driveby.utils.Utils;

public class DevicesInRangeFragment extends AbstractFragment {

    public static final String TAG = DevicesInRangeFragment.class.getSimpleName();
    private static final int REQUEST_LOCATION = 1;
    private DevicesInRangeAdapter mAdapter;
    private BluetoothService bluetoothService;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Handler uiHandler;
    private Guard guard;
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private ScanCallback scanCallbackLollipop = new ScanCallback() {

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            if (result == null) {
                return;
            }
            byte[] scanBytes = null;
            if (result.getScanRecord() != null) {
                scanBytes = result.getScanRecord().getBytes();
            }
            DevicesInRangeFragment.this.scanResult(result.getDevice(),
                                                   result.getRssi(),
                                                   scanBytes);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(getContext());
            }
            builder.setTitle("Błąd skanowania") //TODO LANG
                    .setMessage("Kod błędu: " + errorCode)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    };

    ;
    private DriveByDevice pairedDevice;
    private BluetoothAdapter.LeScanCallback scanCallbackSupported =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                    DevicesInRangeFragment.this.scanResult(device, rssi, scanRecord);
                }
            };
    private final ServiceConnection mServiceConnection =
            new ServiceConnection() {

                @Override
                public void onServiceConnected(ComponentName componentName, IBinder service) {
                    for (int i = 0; i < 3; i++) {
                        try {
                            bluetoothService = ((BluetoothService.LocalBinder) service).getService();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                bluetoothService.scan(true, scanCallbackLollipop);
                            } else {
                                bluetoothService.scan(true, scanCallbackSupported);
                            }
                            break;
                        } catch (NullPointerException ex) {
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                return;
                            }
                        }
                    }
                }

                @Override
                public void onServiceDisconnected(ComponentName componentName) { //Service disconnects
                    bluetoothService = null; //Not bound to a service
                }
            };

    public DevicesInRangeFragment() {
    }

    public static DevicesInRangeFragment newInstance() {
        DevicesInRangeFragment fragment = new DevicesInRangeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        uiHandler = new Handler(Looper.getMainLooper());
        return inflater.inflate(R.layout.content_devices_in_range, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializePageInfo();
        initializeRecyclerView(savedInstanceState);
        mListener.onFragmentChange(this,
                                   null,
                                   mRecyclerView,
                                   SelectableAdapter.Mode.SINGLE);


        if (checkPermissions()) {
            Log.e(TAG, "PERMISSION GRANTED");
            bindBluetoothService(getContext());
        }

    }

    private boolean checkPermissions() {
        if (!Utils.hasMarshmallow()) {
            return true;
        }
        if (ActivityCompat.checkSelfPermission(getContext(),
                                               android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                               REQUEST_LOCATION);
            return false;
        } else {
            Log.e(TAG, "PERMISSION GRANTED");
            bindBluetoothService(getContext());
            return true;
        }
    }

    private void initializePageInfo() {
        pageInfo = new PageInfo();
        pageInfo.setTitle(getString(R.string.title_devices_in_range));
        pageInfo.setIcon(R.drawable.ic_info_connection);
        pageInfo.setBackground(R.drawable.background_info_blue);
    }

    private void initializeRecyclerView(Bundle savedInstanceState) {
        FlexibleAdapter.useTag("DevicesInRangeAdapter");

        Preferences preferences = new Preferences(getContext());
        int selected = -1;

        ArrayList<DeviceInRangeItem> items = new ArrayList<>();

//        for (int i = 0; i < 6; i++) {
//            String serial = User.getUser().getDevice().getAddress();
//            final DriveByDevice d = new DriveByDevice();
//            d.setAddress("00:11:22:33:44:0" + i);
//            if (d.getAddress().equals(serial)) {
//                selected = i;
//            }
//            d.setName("Fake " + i);
//            d.setOnline(i <= 4);
//            final long time = 500000;
//            DeviceInRangeItem item = new DeviceInRangeItem(d) {
//                @Override
//                public boolean isOld() {
//                    return System.currentTimeMillis() - life > time;
//                }
//            };
//            items.add(item);
//        }
        DriveByDevice userDevice = User.getUser().getDevice();
        if (selected == -1 && !userDevice.isVoid()) {
            items.add(new DeviceInRangeItem(userDevice));
            selected = items.size() - 1;
        }

        mAdapter = new DevicesInRangeAdapter(items, getActivity());
        if (selected != -1) {
            mAdapter.toggleSelection(selected);
        }
        mAdapter.setNotifyMoveOfFilteredItems(true)
                .setAnimationOnForwardScrolling(true);

        mRecyclerView = getView().findViewById(R.id.recycler_view);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(createNewLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true); //Size of RV will not change
        // NOTE: Use default item animator 'canReuseUpdatedViewHolder()' will return true if
        // a Payload is provided. FlexibleAdapter is actually sending Payloads onItemChange.
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new FlexibleItemDecoration(getContext())
//                                                .addItemViewType(R.layout.recycler_header_item, 8, 8, 8, 8)
                                                .addItemViewType(R.layout.recycler_simple_item, 0, 0, 0, 4)
                                                .withSectionGapOffset(24)
                                                .withEdge(true));


        // Add FastScroll to the RecyclerView, after the Adapter has been attached the RecyclerView!!!
        FastScroller fastScroller = getView().findViewById(R.id.fast_scroller);
        fastScroller.addOnScrollStateChangeListener((MainActivity) getActivity());
        mAdapter.setFastScroller(fastScroller);

        // New empty views handling, to set after FastScroller
//        EmptyViewHelper.create(mAdapter,
//                               getView().findViewById(R.id.empty_view),
//                               getView().findViewById(R.id.filter_view),
//                               (EmptyViewHelper.OnEmptyViewListener) getActivity()); // Optional!!

        // More settings
        mAdapter.setLongPressDragEnabled(false)
                .setHandleDragEnabled(false)
                .setSwipeEnabled(false)
                .setStickyHeaderElevation(0)
                .setUnlinkAllItemsOnRemoveHeaders(true)
                // Show Headers at startUp, 1st call, correctly executed, no warning log message!
                .setDisplayHeadersAtStartUp(true)
                .setStickyHeaders(true);
        // Simulate developer 2nd call mistake, now it's safe, not executed, no warning log message!
        //.setDisplayHeadersAtStartUp(true)
        // Simulate developer 3rd call mistake, still safe, not executed, warning log message displayed!
        //.showAllHeaders();

        swipeRefreshLayout = getView().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setEnabled(false);
    }

    void bindBluetoothService(Context context) {
        Intent gattServiceIntent = new Intent(context, BluetoothService.class);
        context.bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            bindBluetoothService(getContext());
        }
    }

    @Override
    public void onDestroyView() {
        if (bluetoothService != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bluetoothService.scan(false, scanCallbackLollipop);
            } else {
                bluetoothService.scan(false, scanCallbackSupported);
            }
        }
        getContext().unbindService(mServiceConnection);
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (guard != null) {
            guard.interrupt();
        }
        guard = new Guard();
        guard.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        guard.interrupt();
        guard = null;
    }

    private void scanResult(final BluetoothDevice device, int rssi, byte[] scanRecord) {
        if (device.getName() != null &&
                device.getName().toLowerCase().startsWith("kayon")) {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    DriveByDevice dd = new DriveByDevice();
                    dd.setName(device.getName());
                    dd.setAddress(device.getAddress());
                    mAdapter.update(new DeviceInRangeItem(dd));
                }
            });
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private class Guard extends Thread {
        @Override
        public void run() {
            while (!isInterrupted()) {
                mAdapter.removeOldest();
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    return;
                }

            }
        }
    }
}
