package pl.kayon.driveby.gui.flexibleAdapter;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.helpers.AnimatorHelper;
import eu.davidea.flexibleadapter.items.IFilterable;
import eu.davidea.flexibleadapter.items.IFlexible;
import eu.davidea.flexibleadapter.items.ISectionable;
import eu.davidea.flexibleadapter.utils.DrawableUtils;
import eu.davidea.flexibleadapter.utils.FlexibleUtils;
import eu.davidea.flipview.FlipView;
import eu.davidea.viewholders.FlexibleViewHolder;
import pl.kayon.driveby.R;
import pl.kayon.driveby.utils.Utils;


public abstract class SimpleItem<K extends  SimpleItem.SimpleViewHolder> extends AbstractItem<K>
        implements ISectionable<K, HeaderItem>, IFilterable<String>, Serializable {

    /* The header of this item */
    protected HeaderItem header;

    public SimpleItem(String id) {
        super(id);
        setDraggable(false);
        setSwipeable(false);
    }

    public SimpleItem(String id, HeaderItem header) {
        this(id);
        this.header = header;
    }

//    @Override
//    public String getSubtitle() {
//        return getId()
//                + (getHeader() != null ? " - " + getHeader().getId() : "")
//                + (getUpdates() > 0 ? " - u" + getUpdates() : "");
//    }

    @Override
    public HeaderItem getHeader() {
        return header;
    }

    @Override
    public void setHeader(HeaderItem header) {
        this.header = header;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.recycler_simple_item;
    }

    @Override
    public void bindViewHolder(FlexibleAdapter<IFlexible> adapter, K holder, int position, List<Object> payloads) {
        Context context = holder.itemView.getContext();

        // Background, when bound the first time
        if (payloads.size() == 0) {
            Drawable drawable = DrawableUtils.getSelectableBackgroundCompat(
                    Color.WHITE, Color.parseColor("#dddddd"), //Same color of divider
                    DrawableUtils.getColorControlHighlight(context));
            DrawableUtils.setBackgroundCompat(holder.itemView, drawable);
            DrawableUtils.setBackgroundCompat(holder.frontView, drawable);
        }

        // Display the current flip status
        if (flipViewHidden) {
            holder.getFlipView().setVisibility(View.GONE);
        } else {
            holder.getFlipView().setVisibility(View.VISIBLE);
        }
        holder.mFlipView.flipSilently(adapter.isSelected(position));

        // In case of any Words in the searchText matches with Title this will be highlighted
        if (adapter.hasFilter()) {
            String filter = adapter.getFilter(String.class);
            FlexibleUtils.highlightWords(holder.title, getTitle(), filter);
            FlexibleUtils.highlightWords(holder.subtitle, getSubtitle(), filter);
        } else {
            holder.title.setText(getTitle());
            holder.subtitle.setText(getSubtitle());
        }
    }

    @Override
    public boolean filter(String constraint) {
        for (String word : constraint.split(FlexibleUtils.SPLIT_EXPRESSION)) {
            if (getTitle().toLowerCase().contains(word)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "SimpleItem[" + super.toString() + "]";
    }

    static public class SimpleViewHolder extends FlexibleViewHolder {

        protected FlipView mFlipView;
        protected TextView title;
        protected TextView subtitle;
        protected ImageView mHandleView;
        protected Context mContext;
        protected View frontView;
        protected View rearLeftView;
        protected View rearRightView;

        boolean swiped = false;

        public SimpleViewHolder(View view, FlexibleAdapter adapter) {
            super(view, adapter);
            this.mContext = view.getContext();
            this.title = view.findViewById(R.id.row_title);
            this.subtitle = view.findViewById(R.id.row_subtitle);
            this.mFlipView = view.findViewById(R.id.row_image);
            this.mFlipView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mAdapter.mItemLongClickListener != null) {
                        mAdapter.mItemLongClickListener.onItemLongClick(getAdapterPosition());
//                        Toast.makeText(mContext, "ImageClick on " + title.getText() + " position " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
                        toggleActivation();
                    }
                }
            });
            this.mHandleView = view.findViewById(R.id.row_accessory);
            setDragHandleView(mHandleView);

            this.frontView = view.findViewById(R.id.front_view);
            this.rearLeftView = view.findViewById(R.id.rear_left_view);
            this.rearRightView = view.findViewById(R.id.rear_right_view);
        }

        @Override
        protected void setDragHandleView(@NonNull View view) {
            if (mAdapter.isHandleDragEnabled()) {
                view.setVisibility(View.VISIBLE);
                super.setDragHandleView(view);
            } else {
                view.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View view) {
//            Toast.makeText(mContext, "Click on " + title.getText() + " position " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
            super.onClick(view);
        }

        @Override
        public boolean onLongClick(View view) {
//            Toast.makeText(mContext, "LongClick on " + title.getText() + " position " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
            return super.onLongClick(view);
        }

        @Override
        public void toggleActivation() {
            super.toggleActivation();
            // Here we use a custom Animation inside the ItemView
            mFlipView.flip(mAdapter.isSelected(getAdapterPosition()));
        }

        @Override
        public float getActivationElevation() {
            return Utils.dpToPx(itemView.getContext(), 4f);
        }

        @Override
        protected boolean shouldActivateViewWhileSwiping() {
            return false;//default=false
        }

        @Override
        protected boolean shouldAddSelectionInActionMode() {
            return false;//default=false
        }

        @Override
        public View getFrontView() {
            return frontView;
        }

        @Override
        public View getRearLeftView() {
            return rearLeftView;
        }

        @Override
        public View getRearRightView() {
            return rearRightView;
        }

        public TextView getTitleView() {
            return title;
        }

        public TextView getSubtitleView() {
            return subtitle;
        }

        public FlipView getFlipView() {
            return mFlipView;
        }

        @Override
        public void scrollAnimators(@NonNull List<Animator> animators, int position, boolean isForward) {
            if (mAdapter.getRecyclerView().getLayoutManager() instanceof GridLayoutManager ||
                    mAdapter.getRecyclerView().getLayoutManager() instanceof StaggeredGridLayoutManager) {
                if (position % 2 != 0)
                    AnimatorHelper.slideInFromRightAnimator(animators, itemView, mAdapter.getRecyclerView(), 0.5f);
                else
                    AnimatorHelper.slideInFromLeftAnimator(animators, itemView, mAdapter.getRecyclerView(), 0.5f);
            } else {
                //Linear layout
                if (mAdapter.isSelected(position))
                    AnimatorHelper.slideInFromRightAnimator(animators, itemView, mAdapter.getRecyclerView(), 0.5f);
                else
                    AnimatorHelper.slideInFromLeftAnimator(animators, itemView, mAdapter.getRecyclerView(), 0.5f);
            }
        }

        @Override
        public void onItemReleased(int position) {
            swiped = (mActionState == ItemTouchHelper.ACTION_STATE_SWIPE);
            super.onItemReleased(position);
        }
    }
}
