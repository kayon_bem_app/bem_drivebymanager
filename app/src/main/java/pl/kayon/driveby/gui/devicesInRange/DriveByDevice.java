package pl.kayon.driveby.gui.devicesInRange;

import java.util.Objects;

/**
 * Created by rafalmodrzynski on 13.01.2018.
 */

public class DriveByDevice implements Cloneable {

//    public static final int PAIRED_OFFLINE = 0;
//    public static final int PAIRED_ONLINE = 1;
//    public static final int OFFLINE = 0;
//    public static final int ONLINE = 1;

    public static final String DEFAULT_NAME = "KayonDbD";
    public static final String VOID_NAME = "VOID";

    private String name = DEFAULT_NAME;
    private String address;
    private boolean online = true;
    private double batteryVoltage;
    private String version;

    public String getName() {
        if (name == null) {
            return "Unknown device";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
        if (!online) {
            version = null;
            batteryVoltage = 0;
        }
    }

    public boolean equalsAddress(DriveByDevice d) {
        return address.equals(d.address);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isVoid() {
        return name.equals(VOID_NAME);
    }

    public double getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(double batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean equalsAllFields(DriveByDevice o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DriveByDevice that = (DriveByDevice) o;
        return online == that.online &&
                Double.compare(that.batteryVoltage, batteryVoltage) == 0 &&
                Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(version, that.version);
    }

    @Override
    protected DriveByDevice clone() {
        try {
            DriveByDevice d = (DriveByDevice) super.clone();
            return d;
        } catch (CloneNotSupportedException ex) {
            throw new InternalError("CloneNotSupportedException: " + ex.getMessage());
        }
    }
}
