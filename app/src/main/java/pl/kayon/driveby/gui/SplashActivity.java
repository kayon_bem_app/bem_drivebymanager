package pl.kayon.driveby.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;

import java.util.Random;

import io.fabric.sdk.android.Fabric;
import pl.kayon.driveby.App;
import pl.kayon.driveby.BuildConfig;
import pl.kayon.driveby.gui.auth.AuthenticatorActivity;
import pl.kayon.driveby.gui.auth.UserDataTask;
import pl.kayon.driveby.gui.devicesInRange.DriveByDevice;
import pl.kayon.driveby.gui.main.MainActivity;
import pl.kayon.driveby.model.Route;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.User;
import pl.kayon.driveby.utils.Preferences;
import pl.kayon.driveby.utils.RequestException;
import pl.kayon.driveby.utils.ResponseException;

/**
 * Created by rafalmodrzynski on 13.01.2018.
 */
public class SplashActivity extends AppCompatActivity {

    boolean offline = false;
    private static final String TAG = SplashActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);


        final Preferences preferences = new Preferences(this);
        String token = preferences.getUserToken();
        long tokenExpires = preferences.getUserTokenExpires();
        if (token != null || offline) {
            final User user = User.loginDefault(token, tokenExpires);
            if (offline) {
                setMockData(user);
                nextMainActivity();
                return;
            }
            new UserDataTask(new UserDataTask.Callback() {
                @Override
                public void onRequestException(RequestException ex) {
                    Log.w(TAG, "onRequestException: " + ex.getMessage());
                    nextAuthenticatorActivity();
                }

                @Override
                public void onResponseException(ResponseException ex) {
                    Log.w(TAG, "onResponseException: " + ex.getMessage());
                    nextAuthenticatorActivity();
                }

                @Override
                public void onResult(Boolean b) {
                    if (b == null) {
                        return;
                    }
                    user.loadDevice(SplashActivity.this);
                    nextMainActivity();
                }

                @Override
                public void onCancel() {
                    nextAuthenticatorActivity();
                }
            }, (App)getApplication()).execute(user);

        } else {
            nextAuthenticatorActivity();
        }
    }

    private void setMockData(User user) {
        user.setName("Rafał Modrzyński");
        user.getTasks().clear();
        DriveByDevice device = new DriveByDevice();
        device.setAddress("00:1E:C0:74:56:9E");
        user.setDevice(device);
        Random rnd = new Random(System.currentTimeMillis());
        for (int i = 1; i < 6; i++) {
            Route r1 = new Route(i * 101);
            r1.setName("Trasa " + 101 * i);
            r1.setDetails("Detale " + 101 * i);
            Task task = new Task();
            task.setId(i * 101);
            task.setRoute(r1);
            task.setUser(user);
            task.setState(Task.State.IN_PROGRESS);
            user.getTasks().add(task);
        }
        for (int i = 0; i < 50; i++) {
            Route r1 = new Route(i);
            r1.setName("Trasa " + i);
            r1.setDetails("Detale " + i);
            Task task = new Task();
            task.setId(i * 101);
            task.setRoute(r1);
            task.setUser(user);
            task.setState(Task.State.TODO);
            user.getTasks().add(task);
        }
    }

    private void nextMainActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void nextAuthenticatorActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), AuthenticatorActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}