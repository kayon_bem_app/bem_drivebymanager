package pl.kayon.driveby.gui.metersInRange;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import eu.davidea.fastscroller.FastScroller;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.SelectableAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flipview.FlipView;
import pl.kayon.driveby.R;
import pl.kayon.driveby.gui.main.MainActivity;
import pl.kayon.driveby.gui.flexibleAdapter.AbstractFragment;
import pl.kayon.driveby.gui.flexibleAdapter.PageInfo;
import pl.kayon.driveby.model.Meter;
import pl.kayon.driveby.model.MetersInRange;

public class MetersInRangeFragment extends AbstractFragment implements MetersInRange.Listener {

    public static final String TAG = MetersInRangeFragment.class.getSimpleName();

    private RefresherThread refresher;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MetersInRangeAdapter mAdapter;
    private Handler handler;

    public MetersInRangeFragment() {
    }

    public static MetersInRangeFragment newInstance() {
        MetersInRangeFragment fragment = new MetersInRangeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        handler = new Handler();
        return inflater.inflate(R.layout.content_meters_in_range, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FlipView.resetLayoutAnimationDelay(true, 1000L);

        initializeRecyclerView(savedInstanceState);
        initializePageInfo();

        mListener.onFragmentChange(this,
                                   null,
                                   mRecyclerView,
                                   SelectableAdapter.Mode.IDLE);

        MetersInRange.getInstance().setListener(this);
        FlipView.stopLayoutAnimation();
    }

    private void initializePageInfo() {
        pageInfo = new PageInfo();
        pageInfo.setTitle(getString(R.string.meters_in_range));
        pageInfo.setIcon(R.drawable.ic_info_scanner);
        pageInfo.setBackground(R.drawable.background_info_orange);
    }

    private void initializeRecyclerView(Bundle savedInstanceState) {
        FlexibleAdapter.useTag("MetersInRangeAdapter");
        ArrayList<MeterInRangeItem> items = new ArrayList<>();
        for (Meter m : MetersInRange.getInstance().all()) {
            items.add(new MeterInRangeItem(m, null));
        }
        mAdapter = new MetersInRangeAdapter(items, getActivity());
        mAdapter.setNotifyMoveOfFilteredItems(true)
                .setAnimationOnForwardScrolling(true);

        mRecyclerView = getView().findViewById(R.id.recycler_view);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(createNewLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true); //Size of RV will not change
        // NOTE: Use default item animator 'canReuseUpdatedViewHolder()' will return true if
        // a Payload is provided. FlexibleAdapter is actually sending Payloads onItemChange.
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new FlexibleItemDecoration(getContext())
//                                                .addItemViewType(R.layout.recycler_header_item, 8, 8, 8, 8)
                                                .addItemViewType(R.layout.recycler_simple_item, 0, 0, 0, 4)
                                                .withSectionGapOffset(24)
                                                .withEdge(true));


        // Add FastScroll to the RecyclerView, after the Adapter has been attached the RecyclerView!!!
        FastScroller fastScroller = getView().findViewById(R.id.fast_scroller);
        fastScroller.addOnScrollStateChangeListener((MainActivity) getActivity());
        mAdapter.setFastScroller(fastScroller);

        // New empty views handling, to set after FastScroller
//        EmptyViewHelper.create(mAdapter,
//                               getView().findViewById(R.id.empty_view),
//                               getView().findViewById(R.id.filter_view),
//                               (EmptyViewHelper.OnEmptyViewListener) getActivity()); // Optional!!

        // More settings
        mAdapter.setLongPressDragEnabled(false)
                .setHandleDragEnabled(false)
                .setSwipeEnabled(false)
                .setStickyHeaderElevation(0)
                .setUnlinkAllItemsOnRemoveHeaders(true)
                // Show Headers at startUp, 1st call, correctly executed, no warning log message!
                .setDisplayHeadersAtStartUp(true)
                .setStickyHeaders(true);
        // Simulate developer 2nd call mistake, now it's safe, not executed, no warning log message!
        //.setDisplayHeadersAtStartUp(true)
        // Simulate developer 3rd call mistake, still safe, not executed, warning log message displayed!
        //.showAllHeaders();

        swipeRefreshLayout = getView().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        refresher = new RefresherThread();
        refresher.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        synchronized (this) {
            refresher.interrupt();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MetersInRange.getInstance().removeListener(this);
    }

    @Override
    public void wasChanged() {
    }

    private class RefresherThread extends Thread {
        @Override
        public void run() {
            try {
                while (!isInterrupted()) {
                    Thread.sleep(1000);
                    Log.d(TAG, "refresh adapter");
                    synchronized (MetersInRangeFragment.this) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.refresh();
                            }
                        });
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
