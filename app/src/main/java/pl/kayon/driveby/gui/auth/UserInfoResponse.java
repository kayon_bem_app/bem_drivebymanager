package pl.kayon.driveby.gui.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rafalmodrzynski on 22.02.2018.
 */

public class UserInfoResponse {

    @SerializedName("login")
    private String login;

    @SerializedName("exp")
    private long tokenExpires;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public long getTokenExpires() {
        return tokenExpires;
    }

    public void setTokenExpires(long tokenExpires) {
        this.tokenExpires = tokenExpires;
    }
}
