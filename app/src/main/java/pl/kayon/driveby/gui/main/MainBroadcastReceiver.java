package pl.kayon.driveby.gui.main;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.List;

import pl.kayon.driveby.R;
import pl.kayon.driveby.bluetooth.BluetoothService;
import pl.kayon.driveby.mbus.FrameBuilder;
import pl.kayon.driveby.mbus.FrameBuilderException;
import pl.kayon.driveby.mbus.KayonFrame;
import pl.kayon.driveby.mbus.MeterFrame;
import pl.kayon.driveby.mbus.StatusFrame;
import pl.kayon.driveby.model.Meter;
import pl.kayon.driveby.model.MetersInRange;
import pl.kayon.driveby.model.User;
import pl.kayon.driveby.utils.FileLogger;

public class MainBroadcastReceiver extends BroadcastReceiver {
    public static final String TAG = MainBroadcastReceiver.class.getSimpleName();
    private final MainActivity mainActivity;
    private boolean connected = false;
    private MainServiceConnection serviceConnection = new MainServiceConnection();
    private ImageView statusView;

    public MainBroadcastReceiver(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        //Create intent filter for actions received by broadcast receiver
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothService.ACTION_DATA_WRITTEN);
        return intentFilter;
    }

    MainServiceConnection getServiceConnection() {
        return serviceConnection;
    }

    void onResume() {
        statusView = (ImageView) mainActivity.findViewById(R.id.image_status);
        //Register broadcast receiver to handles events fired by the service: connected, disconnected, etc.
        mainActivity.registerReceiver(this, makeGattUpdateIntentFilter());
        serviceConnection.reconnect();
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        //Get the action that was broadcast by the intent that was received
        final String action = intent.getAction();

        if (BluetoothService.ACTION_GATT_CONNECTED.equals(action)) {
            //Service has connected to BLE device
            //Record the new connection state
            connected = true;
            //Update the display to say "Connected"
            User.getUser().getDevice().setOnline(true);
            //TODO przeniesc to do listenera na polu online
            updateConnectionState(R.drawable.ic_status_ok);
            //Force the Options menu to be regenerated to show the disconnect option
            mainActivity.invalidateOptionsMenu();

        } else if (BluetoothService.ACTION_GATT_DISCONNECTED.equals(action)) {
            //Service has disconnected from BLE device
            connected = false;
            //Update the display to say "Disconnected"
            User.getUser().getDevice().setOnline(false);
            //TODO przeniesc to do listenera na polu online
            updateConnectionState(R.drawable.ic_status_communication_error);
            //Force the Options menu to be regenerated to show the connect option
            mainActivity.invalidateOptionsMenu();
            serviceConnection.reconnect();

        } else if (BluetoothService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
            //Service has discovered GATT services on BLE device
            findMldpGattService(serviceConnection.getBluetoothService().getSupportedGattServices());
            //Show all the supported services and characteristics on the user interface
        } else if (BluetoothService.ACTION_DATA_AVAILABLE.equals(action)) {
            //Service has found new data available on BLE device
            byte[] dataValue = intent.getByteArrayExtra(BluetoothService.EXTRA_DATA);
            //Get the value of the characteristic
            processIncomingPacket(dataValue);
            //Process the data that was received
        } else if (BluetoothService.ACTION_DATA_WRITTEN.equals(action)) {
            //Service has found new data available on BLE device
            //For information only. This application sends small packets infrequently
            // and does not need to know what the previous write completed
        }
    }

    private void updateConnectionState(final int resourceId) {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                statusView.setImageDrawable(mainActivity.getResources().getDrawable(resourceId));
            }
        });
    }

    private void processIncomingPacket(byte[] data) {
        Log.d(TAG, "processIncomingPacket:" + data.length);
        StringBuilder b = new StringBuilder();
        for (byte d : data) {
            b.append(String.format("%02X ", (d & 0xFF)));
        }
        Log.d(TAG, "processIncomingPacket: " + b.toString());
        FileLogger.getInstance().log(b.toString());
        try {
            for (KayonFrame f : FrameBuilder.getInstance().append(data)) {
                if (f instanceof MeterFrame) {
                    MeterFrame mf = (MeterFrame)f;
                    Meter meter = mf.getMeter();
                    MetersInRange.getInstance().add(meter);
                    Log.d(TAG, String.format("RSSI %d %s", mf.getRssi(), mf.getMbusData().toString()));
                } else if (f instanceof StatusFrame) {
                    StatusFrame sf = (StatusFrame)f;
                    User.getUser().getDevice().setVersion(sf.getVersion());
                    User.getUser().getDevice().setBatteryVoltage(sf.getBatteryVoltage());
                }
            }
        } catch (FrameBuilderException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
//        char switchState;
//        int indexStart, indexEnd;
//        incomingMessage = incomingMessage.concat(data);                                 //Add the new data to what is left of previous data
//        if (incomingMessage.length() >= 6 && incomingMessage.contains("=>S") && incomingMessage.contains("\r\n")) { //See if we have the right nessage
//            indexStart = incomingMessage.indexOf("=>S");                                //Get the position of the matching characters
//            indexEnd = incomingMessage.indexOf("\r\n");                                 //Get the position of the end of frame "\r\n"
//            if (indexEnd - indexStart == 4) {                                           //Check that the packet does not have missing or extra characters
//                switchState = incomingMessage.charAt(indexStart + 3);                   //Get the character that represents the switch being pressed
//                if (switchState == '1') {                                               //Is it a "1"
//                    updateDieState();                                                   // if so then update the state of the die with a new roll and send over BLE
//                }
//            }
//            incomingMessage = incomingMessage.substring(indexEnd + 2);                  //Thow away everything up to and including "\n\r"
//        }
//        else if (incomingMessage.contains("\r\n")) {                                    //See if we have an end of frame "\r\n" without a valid message
//            incomingMessage = incomingMessage.substring(incomingMessage.indexOf("\r\n") + 2); //Thow away everything up to and including "\n\r"
//        }
    }

    private void findMldpGattService(List<BluetoothGattService> gattServices) {
        if (gattServices == null) {
            //Verify that list of GATT services is valid
            Log.d(TAG, "findMldpGattService found no Services");
            return;
        }
        //String to compare received UUID with desired known UUIDs
        String uuid;
        //Searching for a characteristic, start with null value
        BluetoothGattCharacteristic mDataMDLP = null;

        //Test each service in the list of services
        for (BluetoothGattService gattService : gattServices) {
            //Get the string version of the service's UUID
            uuid = gattService.getUuid().toString();
            //See if it matches the UUID of the MLDP service
            if (uuid.equals(BluetoothService.MLDP_PRIVATE_SERVICE)) {
                //If so then get the service's list of characteristics
                List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
                //Test each characteristic in the list of characteristics
                for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                    //Get the string version of the characteristic's UUID
                    uuid = gattCharacteristic.getUuid().toString();
                    //See if it matches the UUID of the MLDP data characteristic
                    if (uuid.equals(BluetoothService.MLDP_DATA_PRIVATE_CHAR)) {
                        //If so then save the reference to the characteristic
                        mDataMDLP = gattCharacteristic;
                        Log.d(TAG, "Found MLDP data characteristics");
                    } else if (uuid.equals(BluetoothService.MLDP_CONTROL_PRIVATE_CHAR)) {
                        //See if UUID matches the UUID of the MLDP control characteristic
                        //BluetoothGattCharacteristic controlMLDP = gattCharacteristic;
                        //If so then save the reference to the characteristic
                        Log.d(TAG, "Found MLDP control characteristics");
                    }
                    //Get the properties of the characteristic
                    final int characteristicProperties = gattCharacteristic.getProperties();
                    //See if the characteristic has the Notify property
                    if ((characteristicProperties & (BluetoothGattCharacteristic.PROPERTY_NOTIFY)) > 0) {
                        //If so then enable notification in the BluetoothGatt
                        serviceConnection.getBluetoothService().setCharacteristicNotification(gattCharacteristic, true);
                    }
                    //See if the characteristic has the Indicate property
                    if ((characteristicProperties & (BluetoothGattCharacteristic.PROPERTY_INDICATE)) > 0) {
                        //If so then enable notification (and indication) in the BluetoothGatt
                        serviceConnection.getBluetoothService().setCharacteristicIndication(gattCharacteristic, true);
                    }
                    //See if the characteristic has the Write (acknowledged) property
                    if ((characteristicProperties & (BluetoothGattCharacteristic.PROPERTY_WRITE)) > 0) {
                        //If so then set the write type (write with acknowledge) in the BluetoothGatt
                        gattCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                    }
                    //See if the characteristic has the Write (unacknowledged) property
                    if ((characteristicProperties & (BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) > 0) {
                        //If so then set the write type (write with no acknowledge) in the BluetoothGatt
                        gattCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                    }
                }
                break;
            }
        }
        //See if the MLDP data characteristic was not found
        if (mDataMDLP == null) {
            //If so then show an error message
            //Toast.makeText(mainActivity, R.string.mldp_not_supported, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "findMldpGattService found no MLDP service");
            //and end the activity
        }
    }
}
