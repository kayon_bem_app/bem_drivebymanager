package pl.kayon.driveby.gui.main;

import android.app.AlertDialog;
import android.bluetooth.BluetoothGatt;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.MenuRes;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import eu.davidea.fastscroller.FastScroller;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.SelectableAdapter.Mode;
import eu.davidea.flexibleadapter.helpers.ActionModeHelper;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import pl.kayon.driveby.R;
import pl.kayon.driveby.bluetooth.BluetoothService;
import pl.kayon.driveby.gui.auth.AuthenticatorActivity;
import pl.kayon.driveby.gui.devicesInRange.DeviceInRangeItem;
import pl.kayon.driveby.gui.devicesInRange.DevicesInRangeAdapter;
import pl.kayon.driveby.gui.devicesInRange.DevicesInRangeFragment;
import pl.kayon.driveby.gui.devicesInRange.DriveByDevice;
import pl.kayon.driveby.gui.flexibleAdapter.AbstractFragment;
import pl.kayon.driveby.gui.flexibleAdapter.OnFragmentInteractionListener;
import pl.kayon.driveby.gui.flexibleAdapter.PageInfo;
import pl.kayon.driveby.gui.metersInRange.MetersInRangeFragment;
import pl.kayon.driveby.gui.routes.RouteTaskDetailsTask;
import pl.kayon.driveby.gui.routes.TaskDetailsFragment;
import pl.kayon.driveby.gui.routes.TaskItem;
import pl.kayon.driveby.gui.routes.TaskResultsFragment;
import pl.kayon.driveby.gui.routes.TaskResultsTask;
import pl.kayon.driveby.gui.routes.TasksAdapter;
import pl.kayon.driveby.gui.routes.TasksFragment;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.User;
import pl.kayon.driveby.utils.AsyncTaskCallback;
import pl.kayon.driveby.utils.Preferences;
import pl.kayon.driveby.utils.Utils;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
                   FastScroller.OnScrollStateChangeListener,
                   FlexibleAdapter.OnItemClickListener,
                   FlexibleAdapter.OnItemLongClickListener,
                   OnFragmentInteractionListener {
    public static final String TAG = MainActivity.class.getSimpleName();

    private BluetoothGatt mBluetoothGatt;
    private Handler mHandler;
    private final MainBroadcastReceiver mGattUpdateReceiver = new MainBroadcastReceiver(this);
    private NavigationView navigationView;
    private DrawerLayout mDrawer;
    private Toolbar mToolbar;

    private RecyclerView mRecyclerView;
    private FlexibleAdapter<? extends AbstractFlexibleItem> mAdapter;
    private ActionModeHelper mActionModeHelper;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private AbstractFragment mFragment;
    private ActionBarDrawerToggle drawerToggle;
    private boolean toolBarNavigationListenerIsRegistered = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Utils.hasLollipop()) {
            requestWindowFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }
        super.onCreate(savedInstanceState);

        if (Utils.hasLollipop()) {
            getWindow().setEnterTransition(new Fade());
        }

        setContentView(R.layout.activity_main);

        initializeToolbar();
        initializeDrawer();
        // Initialize Fragment containing Adapter & RecyclerView
        //initializeFragment(savedInstanceState);

        mHandler = new Handler();

        Intent gattServiceIntent = new Intent(this, BluetoothService.class);
        bindService(gattServiceIntent, mGattUpdateReceiver.getServiceConnection(), BIND_AUTO_CREATE);

        showHome();
    }

    private void initializeToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void initializeDrawer() {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerToggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void initializeActionModeHelper(@Mode int mode, @MenuRes int cabMenu) {
        mActionModeHelper = new ActionModeHelper(mAdapter, cabMenu, null) {
            @Override
            public void updateContextTitle(int count) {
                if (mActionMode != null) {//You can use the internal ActionMode instance
                    mActionMode.setTitle(count == 1 ?
                                         getString(R.string.action_selected_one, Integer.toString(count)) :
                                         getString(R.string.action_selected_many, Integer.toString(count)));
                }
            }
        }.withDefaultMode(mode)
                .disableDragOnActionMode(false)
                .disableSwipeOnActionMode(false);
    }

    private void initializePageInfo(PageInfo pageInfo) {
        TextView backgroundView = (TextView) findViewById(R.id.info_background);
        ImageView imageView = (ImageView) findViewById(R.id.info_icon);
        TextView userInfoTextView = (TextView) findViewById(R.id.info_text);
        userInfoTextView.setText(pageInfo.getTitle());
        backgroundView.setBackgroundResource(pageInfo.getBackground());
        imageView.setImageResource(pageInfo.getIcon());
        if (pageInfo.getToolbarTitle() != null) {
            showTitle(pageInfo.getToolbarTitle());
        }
    }

    private void initializeSwipeToRefresh() {
        if (mSwipeRefreshLayout == null) {
            return;
        }
        // Swipe down to force synchronize
        //mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setDistanceToTriggerSync(390);
        mSwipeRefreshLayout.setEnabled(false); //Controlled by fragments!
        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_purple, android.R.color.holo_blue_light,
                android.R.color.holo_green_light, android.R.color.holo_orange_light);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                int a = 10;
                // Passing true as parameter we always animate the changes between the old and the new data set
//                DatabaseService.getInstance().updateNewItems();
//                mRefreshHandler.sendEmptyMessage(REFRESH_START);
//                mRefreshHandler.sendEmptyMessageDelayed(REFRESH_STOP_WITH_UPDATE, 1500L); //Simulate network time
//                mActionModeHelper.destroyActionModeIfCan();
            }
        });
    }

//    private void initializeFragment(Bundle savedInstanceState) {
//        if (savedInstanceState != null) {
////            mFragment = getSupportFragmentManager().getFragment(savedInstanceState, STATE_ACTIVE_FRAGMENT);
//        }
//        if (mFragment != null) {
////            mFragment = FragmentOverall.newInstance(2);
////        }
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.flFragments,
//                                                       mFragment).commit();
//        }
//    }

    public void showTitle(CharSequence title) {
        ((TextView) findViewById(R.id.toolbar_title)).setText(title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGattUpdateReceiver.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);                                        //Activity paused so unregister the broadcast receiver
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Activity ending so unbind the service (this will end the service if no other activities are bound to it)
        unbindService(mGattUpdateReceiver.getServiceConnection());
        mGattUpdateReceiver.getServiceConnection().onDestroy();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();
        if (count > 0) {
            if (count == 1) {
                //pytanie o to czy na pewno ?
            }
            fragmentManager.popBackStack();
            updateBackButton(count > 1);
            return;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mFragment != null) {
            getMenuInflater().inflate(mFragment.getOptionsMenuResId(), menu);
            return true;
        } else {
            return super.onCreateOptionsMenu(menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.action_task_restart: {
                onOptionRestartTask(item);
            }
            return true;
            case R.id.action_task_end: {
                onOptionEndTask(item);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment fragment = new Fragment();
        int id = item.getItemId();
        showTitle(item.getTitle());
        TextView backgroundView = (TextView) findViewById(R.id.info_background);
        ImageView imageView = (ImageView) findViewById(R.id.info_icon);
        TextView textView = (TextView) findViewById(R.id.info_text);

        if (id == R.id.nav_routes) {
            showHome();

        } else if (id == R.id.nav_ble_scanner) {
            showDevicesInRange();

        } else if (id == R.id.nav_scanner) {
            showMetersInRange();

        } else if (id == R.id.menu_logout) {
            User.logout();
            Preferences p = new Preferences(this);
            p.setUserToken(null);
            Intent intent = new Intent(this, AuthenticatorActivity.class);
            intent.putExtra("finish", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showHome() {
        resetBackButton();

        showTitle(getString(R.string.title_route_selection));
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        Fragment fragment = TasksFragment.newInstance(1);
        transaction.replace(R.id.flFragments, fragment);
        transaction.commit();
        manager.executePendingTransactions();
    }

    private void showMetersInRange() {
        showTitle(getString(R.string.title_metters_in_range));
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        Fragment fragment = MetersInRangeFragment.newInstance();
        transaction.replace(R.id.flFragments, fragment);
        transaction.commit();
        manager.executePendingTransactions();
        updateBackButton();
    }

    private void showDevicesInRange() {
        showTitle(getString(R.string.menu_devices_in_range));
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        Fragment fragment = DevicesInRangeFragment.newInstance();
        transaction.replace(R.id.flFragments, fragment);
        transaction.commit();
        manager.executePendingTransactions();

        updateBackButton();
    }

    private void updateBackButton() {
        updateBackButton(getSupportFragmentManager().getBackStackEntryCount() > 0);
    }

    private void resetBackButton() {
        FragmentManager fm = getSupportFragmentManager();
//        int count = fm.getBackStackEntryCount();
//        for(int i = 0; i < count; ++i) {
//            fm.popBackStackImmediate();
//        }
        fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        updateBackButton();
    }

    private void updateBackButton(boolean show) {
        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if (show) {
            // Remove hamburger
            drawerToggle.setDrawerIndicatorEnabled(false);
            // Show back button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if (!toolBarNavigationListenerIsRegistered) {
                drawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Doesn't have to be onBackPressed
                        onBackPressed();
                    }
                });

                toolBarNavigationListenerIsRegistered = true;
            }

        } else {
            // Remove back button
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            // Show hamburger
            drawerToggle.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer drawerToggle listener
            drawerToggle.setToolbarNavigationClickListener(null);
            toolBarNavigationListenerIsRegistered = false;
        }

        // So, one may think "Hmm why not simplify to:
        // .....
        // getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        // mDrawer.setDrawerIndicatorEnabled(!enable);
        // ......
        // To re-iterate, the order in which you enable and disable views IS important #dontSimplify.
    }

    @Override
    public void onFastScrollerStateChange(boolean scrolling) {
        if (scrolling) {
//            hideFab();
        } else {
//            showFab();
        }
    }

    @Override
    public void onFragmentChange(AbstractFragment fragment, SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView, int mode) {
        mFragment = fragment;
        mRecyclerView = recyclerView;
        if (recyclerView != null) {
            mAdapter = (FlexibleAdapter) recyclerView.getAdapter();
        } else {
            mAdapter = null;
        }
        mSwipeRefreshLayout = swipeRefreshLayout;
        initializeSwipeToRefresh();
        initializeActionModeHelper(mode, fragment.getContextMenuResId());
        initializePageInfo(fragment.getPageInfo());
        updateBackButton();
    }

    @Override
    public void initSearchView(Menu menu) {

    }

    @Override
    public boolean onItemClick(View view, int position) {
        try {
            if (mFragment instanceof TaskDetailsFragment) {
                Task task = ((TaskDetailsFragment) mFragment).getTask();
                TaskResultsFragment.Filter filter = TaskResultsFragment.Filter.values()[position];
                addFragmentToStack(filter.getLocalizableName(this),
                                   TaskResultsFragment.newInstance(task, filter));
            } else if (mAdapter instanceof TasksAdapter) {
                final TaskItem item = ((TasksAdapter) mAdapter).getItem(position);
                onSelect(item);

            } else if (mFragment instanceof DevicesInRangeFragment) {
                Preferences preferences = new Preferences(this);
                DeviceInRangeItem item = (DeviceInRangeItem) mAdapter.getItem(position);
                preferences.setDeviceSerial(item.getDevice().getAddress());
                ((DevicesInRangeAdapter)mAdapter).resetLifeSelected();
                boolean isSelected = mAdapter.isSelected(position);
                if (!isSelected) {
                    //przelaczenie na inny
                    mAdapter.toggleSelection(position);
                    DriveByDevice device = new DriveByDevice();
                    device.setName(item.getDevice().getName());
                    device.setAddress(item.getDevice().getAddress());
                    device.setOnline(false);
                    User.getUser().setDevice(device);
                } else {
                    //usuniecie zaznaczenia
                    mAdapter.removeSelection(position);
                    User.getUser().setDevice(null);
                }

            }
        }catch (ClassCastException ex) {
            ex.printStackTrace();
            //czasem item jest HeaderItem - wtedy wyjatek
        }
        return true;
    }

    @Override
    public void onItemLongClick(int position) {
        mActionModeHelper.onLongClick(this, position);
    }

    private void addFragmentToStack(final String title, final Fragment fragment) {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                showTitle(title);

                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out);
                //transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                transaction.replace(R.id.flFragments, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
                manager.executePendingTransactions();
                updateBackButton();
            }
        });
    }

    public void onSelect(final TaskItem item) {
        @StringRes int askMessage = -1;
        if (item.getTask().getState() == Task.State.DONE) {
            askMessage = R.string.are_you_sure_route_restart;
        } else if (item.getTask().getState() == Task.State.TODO){
            askMessage = -1;
        } else {
            askMessage = -1;
        }
        if (askMessage != -1) {
            //pytanie czy chcesz rozpoczac trase jeszcze raz
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            onSelected(item.getTask());
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(askMessage).setPositiveButton(R.string.action_yes, dialogClickListener)
                    .setNegativeButton(R.string.action_no, dialogClickListener).show();
        } else {
            onSelected(item.getTask());
        }
    }
    public void onSelected(final Task task) {
        if (task.getState() != Task.State.IN_PROGRESS) {
            task.getEntities().clear();
            task.setState(Task.State.IN_PROGRESS);
            task.setEnded(null);
            new TaskResultsTask(new AsyncTaskCallback.ActivityDefault(
                    MainActivity.this,
                    R.string.msg_change_task_state) {
                @Override
                public void onResult(Boolean b) {
                    super.onResult(b);
                    if (b) {
                        onSelected(task);
                    }
                }
            }).execute(task);
            return;
        }
        new RouteTaskDetailsTask(new AsyncTaskCallback.ActivityDefault(
                this,
                R.string.msg_download_route_details){
            @Override
            public void onResult(Boolean b) {
                super.onResult(b);
                if (b) {
                    //wyswietlenie trasy
                    addFragmentToStack(getString(R.string.title_route_details),
                                       TaskDetailsFragment.newInstance(task));

                }
            }
        }).execute(task);
    }

    private void onOptionRestartTask(final MenuItem item) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Task task = ((TaskDetailsFragment) mFragment).getTask();
                        task.setEnded(null);
                        task.setState(Task.State.IN_PROGRESS);
                        task.getEntities().clear();
                        new TaskResultsTask(new AsyncTaskCallback.ActivityDefault(
                                MainActivity.this,
                                R.string.msg_change_task_state)).execute(task);
                        mFragment.onOptionsItemSelected(item);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.are_you_sure_route_restart).setPositiveButton(R.string.action_yes, dialogClickListener)
                .setNegativeButton(R.string.action_no, dialogClickListener).show();
    }

    private void onOptionEndTask(final MenuItem item) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Task task = ((TaskDetailsFragment) mFragment).getTask();
                        task.setEnded(new Date());
                        task.setState(Task.State.DONE);
                        new TaskResultsTask(new AsyncTaskCallback.ActivityDefault(
                                MainActivity.this,
                                R.string.msg_upload_task_results) {

                            @Override
                            public void onResult(Boolean b) {
                                super.onResult(b);
                                mFragment.onOptionsItemSelected(item);
                                if (b) {
                                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                }
                            }
                        }).execute(task);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.are_you_sure_route_end).setPositiveButton(R.string.action_yes, dialogClickListener)
                .setNegativeButton(R.string.action_no, dialogClickListener).show();
    }
}
