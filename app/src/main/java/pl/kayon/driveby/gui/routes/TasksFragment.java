package pl.kayon.driveby.gui.routes;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import eu.davidea.fastscroller.FastScroller;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.SelectableAdapter;
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration;
import eu.davidea.flipview.FlipView;
import pl.kayon.driveby.R;
import pl.kayon.driveby.gui.main.MainActivity;
import pl.kayon.driveby.gui.flexibleAdapter.AbstractFragment;
import pl.kayon.driveby.gui.flexibleAdapter.HeaderItem;
import pl.kayon.driveby.gui.flexibleAdapter.PageInfo;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.User;

public class TasksFragment extends AbstractFragment {

    public static final String TAG = TasksFragment.class.getSimpleName();
    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView userInfoTextView;
    private TasksAdapter mAdapter;
    private RecyclerView mRecyclerView;

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static TasksFragment newInstance(int columnCount) {
        TasksFragment fragment = new TasksFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_routes, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FlipView.resetLayoutAnimationDelay(true, 1000L);

        initializeRecyclerView(savedInstanceState);
        initializePageInfo();

        mListener.onFragmentChange(this, swipeRefreshLayout, mRecyclerView, SelectableAdapter.Mode.IDLE);

        FlipView.stopLayoutAnimation();
    }

    private void initializePageInfo() {
        pageInfo = new PageInfo();
        pageInfo.setToolbarTitle(getContext().getResources().getString(R.string.title_route_selection));
        pageInfo.setTitle(User.getUser().getName());
        pageInfo.setBackground(R.drawable.background_info_green);
        pageInfo.setIcon(R.drawable.ic_info_user);

//            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
//        mRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
//        recyclerView.addOnItemTouchListener(
//                new RecyclerItemClickListener(context, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        // do whatever
//                        Log.d(TAG, "onItemClick: ");
//
//                        if (mListener != null) {
//                            mListener.onSelected(User.getUser().getRoutes().get(position));
//                        }
//                    }
//
//                    @Override
//                    public void onLongItemClick(View view, int position) {
//                    }
//                }));
//        if (mColumnCount <= 1) {
//            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
//        } else {
//            mRecyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
//        }
//        recyclerView.setAdapter(new TasksAdapter(User.getUser().getRoutes(), mListener));
    }

    private void initializeRecyclerView(Bundle savedInstanceState) {
        // Initialize Adapter and RecyclerView
        // ExampleAdapter makes use of stableIds, I strongly suggest to implement 'item.hashCode()'
        FlexibleAdapter.useTag("TasksAdapter");

        TaskItem.Header h1 = new TaskItem.Header("1");
        h1.setTitle(getString(R.string.tasks_in_progress_header));

        TaskItem.Header h2 = new TaskItem.Header("2");
        h2.setTitle(getString(R.string.tasks_todo_header));

        TaskItem.Header h3 = new TaskItem.Header("3");
        h3.setTitle(getString(R.string.tasks_done_header));

        TreeSet<TaskItem> items = new TreeSet<>(new Comparator<TaskItem>() {
            @Override
            public int compare(TaskItem o1, TaskItem o2) {
                int c = o1.getHeader().getId().compareTo(o2.getHeader().getId());
                if (c != 0) {
                    return c;
                }
                return o1.getTask().getRoute().getName().compareTo(o2.getTask().getRoute().getName());
            }
        });
        for (Task task : User.getUser().getTasks()) {
            TaskItem.Header header;
            if (task.getState() == Task.State.IN_PROGRESS) {
                header = h1;
            } else if (task.getState() == Task.State.DONE) {
                header = h3;
            } else {
                header = h2;
            }
            TaskItem ri = new TaskItem(task, header);
            items.add(ri);
        }
        mAdapter = new TasksAdapter(new ArrayList<TaskItem>(items), getActivity());

        mAdapter.setNotifyMoveOfFilteredItems(true)
                .setAnimationOnForwardScrolling(true);

        mRecyclerView = getView().findViewById(R.id.recycler_view);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(createNewLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true); //Size of RV will not change
        // NOTE: Use default item animator 'canReuseUpdatedViewHolder()' will return true if
        // a Payload is provided. FlexibleAdapter is actually sending Payloads onItemChange.
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new FlexibleItemDecoration(getContext())
                                                .addItemViewType(R.layout.recycler_header_item, 10, 0, 10, 0)
                                                .addItemViewType(R.layout.recycler_simple_item, 10, 0, 10, 1)
                                                .withSectionGapOffset(10)
                                                .withEdge(true));


        // Add FastScroll to the RecyclerView, after the Adapter has been attached the RecyclerView!!!
        FastScroller fastScroller = getView().findViewById(R.id.fast_scroller);
        fastScroller.addOnScrollStateChangeListener((MainActivity) getActivity());
        mAdapter.setFastScroller(fastScroller);

        // New empty views handling, to set after FastScroller
//        EmptyViewHelper.create(mAdapter,
//                               getView().findViewById(R.id.empty_view),
//                               getView().findViewById(R.id.filter_view),
//                               (EmptyViewHelper.OnEmptyViewListener) getActivity()); // Optional!!

        // More settings
        mAdapter.setLongPressDragEnabled(true)
                .setHandleDragEnabled(true)
                .setSwipeEnabled(true)
                .setStickyHeaderElevation(0)
                .setUnlinkAllItemsOnRemoveHeaders(true)
                // Show Headers at startUp, 1st call, correctly executed, no warning log message!
                .setDisplayHeadersAtStartUp(true)
                .setStickyHeaders(true);
        // Simulate developer 2nd call mistake, now it's safe, not executed, no warning log message!
        //.setDisplayHeadersAtStartUp(true)
        // Simulate developer 3rd call mistake, still safe, not executed, warning log message displayed!
        //.showAllHeaders();

        swipeRefreshLayout = getView().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setEnabled(true);

        // Add 2 Scrollable Headers and 1 Footer
//        mAdapter.addUserLearnedSelection(savedInstanceState == null);
//        mAdapter.addScrollableHeaderWithDelay(new ScrollableUseCaseItem(
//                getString(R.string.headers_sections_use_case_title),
//                getString(R.string.headers_sections_use_case_description)), 900L, false);
//        mAdapter.addScrollableFooter();
    }

    @Override
    public int getOptionsMenuResId() {
        return R.menu.menu_empty;
    }

    @Override
    public void onResume() {
        super.onResume();
//        userInfoTextView.setText(User.getUser().getName());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

//    public interface RouteSelectedListener {
//        void onSelected(Route route);
//    }
}
