package pl.kayon.driveby.gui.flexibleAdapter;

import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

public abstract class AbstractItem<VH extends FlexibleViewHolder>
        extends AbstractFlexibleItem<VH> {

    protected String id;
    protected String title;
    protected String subtitle = "";
    protected boolean flipViewHidden = true;
    /* number of times this item has been refreshed */
    protected int updates;

    public AbstractItem(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object inObject) {
        if (inObject instanceof AbstractItem) {
            AbstractItem inItem = (AbstractItem) inObject;
            return this.id.equals(inItem.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public boolean isFlipViewHidden() {
        return flipViewHidden;
    }

    public void setFlipViewHidden(boolean flipViewHidden) {
        this.flipViewHidden = flipViewHidden;
    }

    public int getUpdates() {
        return updates;
    }

    public void increaseUpdates() {
        this.updates++;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", title=" + title;
    }

}