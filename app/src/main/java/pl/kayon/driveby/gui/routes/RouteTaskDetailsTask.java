package pl.kayon.driveby.gui.routes;

import android.os.AsyncTask;

import java.util.ArrayList;

import pl.kayon.driveby.model.Meter;
import pl.kayon.driveby.model.Task;
import pl.kayon.driveby.model.TaskEntity;
import pl.kayon.driveby.model.User;
import pl.kayon.driveby.utils.AsyncTaskCallback;
import pl.kayon.driveby.utils.NetworkUtilities;
import pl.kayon.driveby.utils.RequestException;
import pl.kayon.driveby.utils.ResponseException;

/**
 * Created by rafalmodrzynski on 26.01.2018.
 */

public class RouteTaskDetailsTask extends AsyncTask<Task, Void, Boolean> {

    private final AsyncTaskCallback callback;

    public RouteTaskDetailsTask(AsyncTaskCallback callback) {
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        callback.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Task... tasks) {
        try {
            User user = User.getUser();
            for (Task t : tasks) {
                t.setRoute(NetworkUtilities.getInstance().getRouteDetails(user, t.getRoute()));
                ArrayList<TaskEntity> toRemove = new ArrayList<>();
                for (TaskEntity te : t.getEntities()) {
                    if (te.getMeter() instanceof Meter.Temp) {
                        Meter m = t.getRoute().findMeterById(te.getMeter().getId());
                        if (m == null) {
                            toRemove.add(te);
                        }
                        te.setMeter(m);
                    }
                }
                t.getEntities().removeAll(toRemove);
//                NetworkUtilities.getInstance().getTaskResults(user, t);
            }
            return true;
        } catch (RequestException ex) {
            callback.onRequestException(ex);
        } catch (ResponseException ex) {
            callback.onResponseException(ex);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean b) {
        callback.onResult(b);
    }

    @Override
    protected void onCancelled() {
        callback.onCancel();
    }

}
