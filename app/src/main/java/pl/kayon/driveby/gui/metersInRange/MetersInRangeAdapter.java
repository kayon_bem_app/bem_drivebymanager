package pl.kayon.driveby.gui.metersInRange;

import java.util.List;

import eu.davidea.flexibleadapter.items.IFlexible;
import pl.kayon.driveby.gui.flexibleAdapter.AbstractAdapter;
import pl.kayon.driveby.model.Meter;
import pl.kayon.driveby.model.MetersInRange;

public class MetersInRangeAdapter extends AbstractAdapter<MeterInRangeItem> {

//    private List<Meter> meters;

    public MetersInRangeAdapter(List<MeterInRangeItem> items, Object listeners) {
        super(items, listeners, false);
    }

    public void refresh() {
        for (Meter m : MetersInRange.getInstance().all()) {
            MeterInRangeItem newItem = new MeterInRangeItem(m);
            int pos = getGlobalPositionOf(newItem);
            if (pos == -1) {
                addItem(newItem);
            } else {
                updateItem(newItem);
            }
        }
    }
}
