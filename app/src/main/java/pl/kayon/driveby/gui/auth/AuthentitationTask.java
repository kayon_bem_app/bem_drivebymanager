package pl.kayon.driveby.gui.auth;

import android.os.AsyncTask;
import android.util.Log;

import pl.kayon.driveby.utils.NetworkUtilities;
import pl.kayon.driveby.utils.RequestException;
import pl.kayon.driveby.utils.ResponseException;

/**
 * Created by rafalmodrzynski on 21.01.2018.
 */
public class AuthentitationTask extends AsyncTask<Void, Void, AuthResponse> {

    private static final String TAG = AuthentitationTask.class.getSimpleName();
    private final AuthBody body;
    private final Callback taskCallback;

    public AuthentitationTask(AuthBody body, Callback taskCallback) {
        this.body = body;
        this.taskCallback = taskCallback;
    }

    @Override
    protected AuthResponse doInBackground(Void... params) {
        try {
            return NetworkUtilities.getInstance().authenticate(body);
        } catch (RequestException ex) {
            taskCallback.onRequestException(ex);
            return null;
        } catch (ResponseException ex) {
            taskCallback.onResponseException(ex);
            return null;
        }
    }

    @Override
    protected void onPostExecute(final AuthResponse authToken) {
        taskCallback.onAuthenticationResult(authToken);
    }

    @Override
    protected void onCancelled() {
        taskCallback.onAuthenticationCancel();
    }

    public static interface Callback {

        public void onRequestException(RequestException ex);

        public void onResponseException(ResponseException ex);

        public void onAuthenticationResult(AuthResponse authToken);

        public void onAuthenticationCancel();
    }
}
