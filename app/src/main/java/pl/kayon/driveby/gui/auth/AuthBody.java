package pl.kayon.driveby.gui.auth;

/**
 * Created by rafalmodrzynski on 04.02.2018.
 */

public class AuthBody {
    private final String login;
    private final String password;

    public AuthBody(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
