package pl.kayon.driveby.model;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import pl.kayon.driveby.model.Task;

/**
 * Created by rafalmodrzynski on 7.07.2018.
 */

public class Tasks extends ArrayList<Task> {

    public Task findById(long id) {
        for (Task r: this) {
            if (r.getId() == id) {
                return r;
            }
        }
        throw new NoSuchElementException("No element with id " + id );
    }
    public Task get(int index) {
        synchronized (this) {
            return super.get(index);
        }
    }

    public int size() {
        synchronized (this) {
            return super.size();
        }
    }

    public void clear() {
        synchronized (this) {
            super.clear();
        }
    }
}
