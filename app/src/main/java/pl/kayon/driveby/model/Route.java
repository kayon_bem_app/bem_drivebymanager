package pl.kayon.driveby.model;

/**
 * Created by rafalmodrzynski on 14.01.2018.
 */

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

public class Route {

    @SerializedName("id")
    private final long id;
    @SerializedName("name")
    private String name;
    @SerializedName("details")
    private String details;
    @SerializedName("meters")
    private ArrayList<Meter> meters = new ArrayList<>();
    @SerializedName("created_at")
    private Date createdAt;
    @SerializedName("updated_at")
    private Date updatedAt;

    public Route(long id) {
        this.id = id;
    }

    public static Route createDummy() {
        Route r = new Route(0);
        r.name = "Dummy route";
        return r;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public ArrayList<Meter> getMeters() {
        if (meters == null) {
            meters = new ArrayList<>();
        }
        return meters;
    }

    public Meter findMeterById(long id) {
        for (Meter m: getMeters()) {
            if (m.getId() == id) {
                return m;
            }
        }
        return null;
    }

    public void setMeters(ArrayList<Meter> meters) {
        this.meters = meters;
    }

}