package pl.kayon.driveby.model;

import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class TaskEntities implements Iterable<TaskEntity> {

    private HashMap<Long, TaskEntity> map = new HashMap<>();
    private TaskEntity lastObject;
    private Listener listener;


    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public TaskEntity add(TaskEntity taskEntity) {
        if (taskEntity.getMeter() == null) {
            throw new IllegalArgumentException("Meter is null");
        }
        lastObject = taskEntity;
        TaskEntity old = map.put(taskEntity.getMeter().getId(), taskEntity);
        if (listener != null) {
            if (old != null) {
                listener.wasReplaced(old, taskEntity);
            } else {
                listener.wasAdd(taskEntity);
            }
        }
        return old;
    }

    public void clear() {
        map.clear();
        if (listener != null) {
            listener.wasClear();
        }
    }

    public TaskEntity getLast() {
        return lastObject;
    }

    public int size() {
        return map.size();
    }

    public void removeAll(Collection<TaskEntity> collection) {
        for (TaskEntity t: collection) {
            map.remove(t.getMeter().getId());
        }
    }

    @NonNull
    @Override
    public Iterator<TaskEntity> iterator() {
        return map.values().iterator();
    }

    public static interface Listener {

        public void wasReplaced(TaskEntity oldObject, TaskEntity newObject);

        public void wasAdd(TaskEntity te);

        public void wasClear();
    }
}
