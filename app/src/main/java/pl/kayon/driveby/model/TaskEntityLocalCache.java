package pl.kayon.driveby.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.Date;

@Entity(
        indexes = {
                @Index(value = "taskId,meterId", unique = true)
        }
)
public class TaskEntityLocalCache {
    @Id
    private Long id;
    @NotNull
    private Long taskId;
    @NotNull
    private Long meterId;
    private String result;
    @NotNull
    private Integer rssi;
    @NotNull
    private Date date;

    public TaskEntityLocalCache(TaskEntity te) {
        this.taskId = te.getTask().getId();
        this.meterId = te.getMeter().getId();
        this.result = te.getResult();
        this.rssi = te.getRssi();
        this.date = te.getDate();
    }

    @Generated(hash = 826364183)
    public TaskEntityLocalCache(Long id, @NotNull Long taskId,
                                @NotNull Long meterId, String result, @NotNull Integer rssi,
                                @NotNull Date date) {
        this.id = id;
        this.taskId = taskId;
        this.meterId = meterId;
        this.result = result;
        this.rssi = rssi;
        this.date = date;
    }

    @Generated(hash = 739004736)
    public TaskEntityLocalCache() {
    }

    public TaskEntity toTaskEntity(Task task) {
        if (task.getId() != taskId) {
            throw new IllegalArgumentException("Invalid task");
        }
        TaskEntity te = new TaskEntity();
        te.setTask(task);
        for (Meter m : task.getRoute().getMeters()) {
            if (m.getId() == this.meterId) {
                te.setMeter(m);
                break;
            }
        }
        if (te.getMeter() == null) {
            te.setMeter(new Meter.Temp(this.meterId));
        }
        te.setResult(this.result);
        te.setRssi(this.rssi);
        te.setDate(this.date);
        return te;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getMeterId() {
        return this.meterId;
    }

    public void setMeterId(Long meterId) {
        this.meterId = meterId;
    }

    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getRssi() {
        return this.rssi;
    }

    public void setRssi(Integer rssi) {
        this.rssi = rssi;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
