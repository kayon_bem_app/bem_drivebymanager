package pl.kayon.driveby.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;

import pl.kayon.driveby.model.Route;

/**
 * Created by rafalmodrzynski on 14.01.2018.
 */

public class Routes {

    private final ArrayList<Route> routes = new ArrayList<>();

    public void add(Route route) {
        routes.add(route);
    }

    public void add(Collection<Route> collection) {
        routes.addAll(collection);
    }

    public Route findById(long id) {
        for (Route r: routes) {
            if (r.getId() == id) {
                return r;
            }
        }
        throw new NoSuchElementException("No element with id " + id );
    }
    public Route get(int index) {
        synchronized (routes) {
            try {
                return routes.get(index);
            } catch (ArrayIndexOutOfBoundsException ex) {
                return Route.createDummy();
            }
        }
    }

    public int size() {
        synchronized (routes) {
            return routes.size();
        }
    }

    public void clear() {
        synchronized (routes) {
            routes.clear();
        }
    }
}
