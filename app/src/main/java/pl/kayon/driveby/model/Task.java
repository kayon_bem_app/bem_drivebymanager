package pl.kayon.driveby.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class Task {

    private static SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());

    @SerializedName("id")
    private long id;
    @SerializedName("route")
    private Route route;
    private User user;
    private TaskEntities entities;
    @SerializedName("created")
    private Date created;
    @SerializedName("ended")
    private Date ended;
    @SerializedName("state")
    private State state;

    public static String dateAsString(Date date) {
        return sf.format(date);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TaskEntities getEntities() {
        if (entities == null) {
            entities = new TaskEntities();
        }
        return entities;
    }

    public void setEntities(TaskEntities entities) {
        this.entities = entities;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getEnded() {
        return ended;
    }

    public void setEnded(Date ended) {
        this.ended = ended;
    }

    public boolean isDone() {
        return state == State.DONE;
    }

    public enum State {
        TODO, IN_PROGRESS, DONE
    }

    public static class StateDeserializer implements JsonDeserializer<State> {
        @Override
        public State deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            return State.values()[Integer.parseInt(json.getAsString())];
        }
    }
}
