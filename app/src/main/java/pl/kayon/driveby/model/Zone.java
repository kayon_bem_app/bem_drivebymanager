package pl.kayon.driveby.model;

/**
 * Created by rafalmodrzynski on 14.01.2018.
 */

public class Zone {

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
