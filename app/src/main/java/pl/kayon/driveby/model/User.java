package pl.kayon.driveby.model;

import android.content.Context;

import java.util.ArrayList;

import pl.kayon.driveby.gui.devicesInRange.DriveByDevice;
import pl.kayon.driveby.utils.Preferences;

/**
 * Created by rafalmodrzynski on 13.01.2018.
 */

public class User {

    public static final String PROP_DEVICE = "device";
    private static User ANONYMOUS = createAnonymous();
    private static User user = ANONYMOUS;
    private String name;
    private String token;
    private long tokenExpires;
    private DriveByDevice device;
    private Tasks tasks;
    private ArrayList<Listener> listeners = new ArrayList<>();

    public static User loginDefault(String token, long tokenExpires) {
        User user = new User();
        user.token = token;
        user.tokenExpires = tokenExpires;
        login(user);
        return user;
    }

    public static void login(User user) {
        User.user = user;
    }

    public static void logout() {
        user = ANONYMOUS;
    }

    public static User getUser() {
        return user;
    }

    private static User createAnonymous() {
        User u = new User();
        u.device = null;

        return u;
    }

    public boolean isLogged() {
        return this != ANONYMOUS;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public long getTokenExpires() {
        return tokenExpires;
    }

    public void setTokenExpires(long tokenExpires) {
        this.tokenExpires = tokenExpires;
    }

    public void loadDevice(Context context) {
        final Preferences preferences = new Preferences(context);
        String serial = preferences.getDeviceSerial();
        if (serial != null && serial.length() > 0) {
            DriveByDevice device = new DriveByDevice();
            device.setAddress(serial);
            device.setOnline(false);
            setDevice(device);
        }
    }
    public DriveByDevice getDevice() {
        if (device == null) {
            device = new DriveByDevice();
            device.setName(DriveByDevice.VOID_NAME);
            device.setAddress("00:00:00:00:00:00");
        }
        return device;
    }

    public void setDevice(DriveByDevice device) {
        this.device = device;
        fireChanged(PROP_DEVICE);
    }

    public void addListener(Listener l) {
        listeners.add(l);
    }
    public void removeListener(Listener l) {
        listeners.remove(l);
    }

    private void fireChanged(String property) {
        for (Listener l: listeners) {
            l.onChanged(property);
        }
    }

    public Tasks getTasks() {
        if (tasks == null) {
            tasks = new Tasks();
        }
        return tasks;
    }

    public interface Listener {
        void onChanged(String property);
    }
}
