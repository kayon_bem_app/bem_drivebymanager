package pl.kayon.driveby.model;

import com.google.gson.annotations.SerializedName;

import pl.kayon.driveby.mbus.DeviceType;
import pl.kayon.driveby.mbus.TFormatBFrame;

/**
 * Created by rafalmodrzynski on 14.01.2018.
 */

public class Meter {

    private final long createdAt = System.currentTimeMillis();
    @SerializedName("id")
    private Long id;
    @SerializedName("wmbus")
    private long wmbus;
    @SerializedName("aes_key")
    private String encodingKey;
    @SerializedName("address1")
    private String address1;
    @SerializedName("address2")
    private String address2;
    private TFormatBFrame lastFrame;
    private int rssi;
    private DeviceType type;
    @SerializedName("manufacturer_name")
    private String manufacturerName;
    @SerializedName("ppe_number")
    private String ppeNumber;
    @SerializedName("meter_number")
    private String meterNumber;
    private int version;

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public DeviceType getType() {
        return type;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getUniqueNumber() {
        return manufacturerName + wmbus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getWmbus() {
        return wmbus;
    }

    public void setWmbus(long wmbus) {
        this.wmbus = wmbus;
    }

    public String getEncodingKey() {
        return encodingKey;
    }

    public void setEncodingKey(String encodingKey) {
        this.encodingKey = encodingKey;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public String getGroupName() {
        return address1;
    }

    public String getPpeNumber() {
        return ppeNumber;
    }

    public void setPpeNumber(String ppeNumber) {
        this.ppeNumber = ppeNumber;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public void setLastFrame(TFormatBFrame lastFrame) {
        this.lastFrame = lastFrame;
    }

    public TFormatBFrame getLastFrame() {
        return lastFrame;
    }

    public static class Temp extends Meter {
        public Temp(long id) {
            this.setId(id);
        }
    }
}
