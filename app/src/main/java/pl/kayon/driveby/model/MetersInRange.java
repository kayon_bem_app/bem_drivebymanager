package pl.kayon.driveby.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by rafalmodrzynski on 23.01.2018.
 */

public class MetersInRange {

    private static MetersInRange instance = new MetersInRange();

    private Listener listener;
    private HashMap<String, Meter> meters = new HashMap<>();

    public static MetersInRange getInstance() {
        return instance;
    }

    public List<Meter> all() {
        synchronized (this) {
            TreeSet<Meter> sorted = new TreeSet<Meter>(new DeviceComparator());
            ArrayList<Meter> toRemove = new ArrayList<>();
            for (Meter m : meters.values()) {
                if (m.getCreatedAt() + (5 * 60000) < System.currentTimeMillis()) {
                    toRemove.add(m);
                    continue;
                }
                sorted.add(m);
            }
            for (Meter m : toRemove) {
                meters.remove(getKey(m));
            }
            return Collections.unmodifiableList(new ArrayList<Meter>(sorted));
        }
    }

    public List<Meter>allWithClear() {
        synchronized (this) {
            TreeSet<Meter> sorted = new TreeSet<Meter>(new DeviceComparator());
            for (Meter m : meters.values()) {
                if (m.getCreatedAt() + (5 * 60000) < System.currentTimeMillis()) {
                    continue;
                }
                sorted.add(m);
            }
            meters.clear();
            return Collections.unmodifiableList(new ArrayList<Meter>(sorted));
        }
    }

    private String getKey(Meter m) {
        return String.format("%s-%d:%d", m.getManufacturerName(), m.getWmbus(), m.getVersion());
    }

    public void add(Meter meter) {
        synchronized (this) {
            meters.put(getKey(meter), meter);
        }
        if (listener != null) {
            listener.wasChanged();
        }
    }

    public void setListener(Listener listener) {
        synchronized (this) {
            this.listener = listener;
        }
    }
    public void removeListener(Listener listener) {
        synchronized (this) {
            if (this.listener == listener) {
                this.listener = null;
            }
        }
    }

    public static interface Listener {
        public void wasChanged();
    }

    private static class DeviceComparator implements Comparator<Meter> {
        @Override
        public int compare(Meter o1, Meter o2) {
            int x = o1.getManufacturerName().compareTo(o2.getManufacturerName());
            if (x != 0) {
                return x;
            }
            x =  Long.compare(o1.getWmbus(), o2.getWmbus());
            if (x != 0) {
                return x;
            }
            return Integer.compare(o1.getVersion(), o2.getVersion());
        }

    }
}
